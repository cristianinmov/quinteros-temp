// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  isMockEnabled: true, // You have to switch this, when your real back-end is done
  USER_DEFAULT: "/assets/user_default.jpg",
  BLOG_DEFAULT: "https://dubsism.files.wordpress.com/2017/12/image-not-found.png",
  LOADDING_IMG: "../assets/loadding.gif",
  PUBLICATION_DEFAULT: "../../assets/image_not_found.jpg",
  API: {
    // API_URL: 'https://srvs.mydigitalcard.us/api',
    //API_URL: 'http://192.168.11.128:8009/api',
    //API_URL: "http://201.217.212.69:8000",
    // API_URL: "http://190.131.235.19:8000",
    API_URL:"https://api.losquintero.club/",
    API_LOGIN: "/api/v1/app/login",
    API_ME: "/api/v1/auth/me",
    API_REFRESH_TOKEN: "/api/v1/auth/refresh",
    API_LOGOUT: "/api/v1/auth/logout",
    API_LOGIN_SOCIAL_: "/api/v1/auth/login_social",
    API_REGISTER: "/api/v1/app/company",
    API_FORGOT_PASSWORD: "/api/v1/app/recovery/pass",
    API_FORGOT_PASSWORD_VALIDATE:"/api/v1/app/validate/token",
    API_UUID: "/api/v1/auth/uuid",
    API_SET_LANG: "/api/v1/locale/",
    API_POSITIONS: "/api/v1/position/list/all",
    API_GET_TERMS_AND_CONDITIONS: "/api/v1/terms/get/active",
    API_VALIDATE_TERMS_AND_CONDITIONS: "/api/v1/terms/check/accept",
    API_AGREE_TERMS: "/api/v1/terms/accept/new",
    API_GET_VERSION_APP: "/api/v1/app/version/info",
    AUTH: "",
    LANG: "",
    METHOD: "", //0 email and password and 1 google
    API_USERS: {
      GET_ALL: "/api/v1/user",
      GET_BY_ID: "/api/v1/user/"
    },
    API_SMS_TWILIO: "/api/v1/send/sms",
    API_UPDATE_USER: "/api/v1/user/:id?_method=PUT",
    DIRECTORY:{
      API_GET_BRANCHES: "/api/v1/branches"
    },
    BLOG: {
      BLOG_GET: "/api/v1/news",
      CATEGORIES_GET: "/api/v1/news_category/list/all",
      GET_LIKES_LIKES: "/api/v1/likes/count/",
      POST_ADD_LIKES: "/api/v1/likes/add",
      GET_COMMENTS: "/api/v1/comments/count/",
      // POST_ADD_VIEW: "/api/v1/likes/add/",
      GALLERY_SHOW_GET: "/api/v1/news/gallery/show/"
    },
    COMMUNITY:{
      COMMUNITY_GET: "/api/v1/community",
      //API_GET_BLOKED_USERS: "/api/v1/community/blocked/users",
      API_GET_BLOKED_USERS: "/api/v1/community/blocked/users/me",
      API_POST_UNLOCK_USERS: "/api/v1/community/unblock/users",
      SAVE: '/api/v1/community',
      GET_COMMENTS: "/api/v1/comments/count/",
      GET_ALL_COMMENTARIES: "/api/v1/comments/list/",
      POST_ADD_COMMENT: "/api/v1/comments/add/",
      POST_ADD_LIKES: "/api/v1/community/likes/add",
      POST_REPORT_USER: "/api/v1/community/block/users",
      POST_REPORT_POST: "/api/v1/community/report/post"
    },
    DIGITAL_CARD:{
      // COMMUNITY_GET: "/api/v1/community",
      // SAVE: '/api/v1/community',
      // GET_COMMENTS: "/api/v1/comments/count/",
      GET_CONTACTS: "/api/v1/mdc",
      GET_PREFIX: "/api/v1/global/flags/",
      POST_ADD_CONTACT: "/api/v1/mdc"
      // POST_ADD_LIKES: "/api/v1/community/likes/add"
    }

  },
  LOCAL_STORAGE: {
    PLATAFORM: "",
    LANGUAGE: "",
    UUID: ""
  },
  firebase: {
    apiKey: "AIzaSyBQPDB2sFTYLDw_zpnJmeAbJqIJi7vvn-c",
    authDomain: "my-digital-card-c1364.firebaseapp.com",
    databaseURL: "https://my-digital-card-c1364.firebaseio.com",
    projectId: "my-digital-card-c1364",
    storageBucket: "my-digital-card-c1364.appspot.com",
    messagingSenderId: "181839308556"
  }
};
