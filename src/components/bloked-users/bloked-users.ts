import { LoginPage } from './../../pages/login/login';
import { Component } from '@angular/core';
import { HomePage } from '../../pages/home/home';
import { NavController, LoadingController, AlertController, Events, Platform, ViewController, ActionSheetController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { NavParams } from 'ionic-angular';
import { CommutyProvider } from '../../providers/commuty/commuty';
import { StatusBar } from '@ionic-native/status-bar';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { GeneralProvider } from '../../providers/general/general';
import { ImageLoader } from 'ionic-image-loader';

/**
 * Generated class for the BlokedUsersComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'bloked-users',
  templateUrl: 'bloked-users.html'
})
export class BlokedUsersComponent {

  wait:any;
  AUTH = environment.API;
  MESSAGE_ERROR:any;
  bloked_useres:any = [];
  callback:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public commutyProvider: CommutyProvider,
    public statusBar: StatusBar,
    public storage: Storage,
    public alertCtrl: AlertController,
    public events: Events,
    public photoViewer: PhotoViewer,
    public general: GeneralProvider,
    public translate: TranslateService,
    public platform: Platform,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public imageLoader: ImageLoader,
    public actionsheetCtrl : ActionSheetController,
    public toastCtrl: ToastController
  ) {}

  async ionViewDidEnter(){
    this.callback = this.navParams.get("callback");
    await this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    await this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    this.getAllBlokedUsers();
  }

  async getAllBlokedUsers() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    try {
      const response: any = await this.commutyProvider.getAllBlokedUsers();
      this.bloked_useres = response.data;
      loading.dismiss();
    } catch (e) {
      loading.dismiss();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  async goToHome(){
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: "forward" });
  }
  async unlockUser(user){

    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    var data =
    {
      "user_block_id" : user.id
    };
    try {
      const response: any = await this.commutyProvider.postUnlockUsers(data);
      await this.getAllBlokedUsers();
      const dataCallback = {
        response: data,
        data: this.bloked_useres
      }
      this.callback(dataCallback);
      switch(response.errors.status){
        case 200:
        response.errors.messages.forEach(element => {

            let toast = this.toastCtrl.create({
              message: element,
              position: "button",
              showCloseButton: true,
              closeButtonText: "Ok",
              duration: 4000,
            });

            toast.present(toast);
          });
        break;
      }
      loading.dismiss();
    } catch (e) {
      loading.dismiss();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  async doRefresh(refresher) {
    try {
      this.getAllBlokedUsers();
      refresher.complete();
    } catch (e) {}
  }
}
