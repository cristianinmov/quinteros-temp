import { Storage } from "@ionic/storage";
import { StatusBar } from "@ionic-native/status-bar";
import { environment } from "./../../environments/environment";
import { TranslateService } from "@ngx-translate/core";
import {
  NavParams,
  LoadingController,
  AlertController,
  NavController,
  Platform,
  ViewController,
  ToastController
} from "ionic-angular";
import { DirectoryProvider } from "./../../providers/directory/directory";
import { Component } from "@angular/core";
import { HomePage } from "../../pages/home/home";
// Contacts Native
import {
  Contacts,
  Contact,
  ContactField,
  ContactName,
  ContactOrganization
} from "@ionic-native/contacts";
import introJs from '../../../node_modules/intro.js/intro';
import { GeneralProvider } from "../../providers/general/general";

@Component({
  selector: "directory-contact",
  templateUrl: "directory-contact.html"
})
export class DirectoryContactComponent {
  AUTH = environment.API;
  text: string;
  position: any = {};
  company: any = {};
  wait;
  imgDefault: any = environment.USER_DEFAULT;
  userDirectory: any[] = [];
  image_head: any[] = [];
  user: any = {
    avatar: this.imgDefault
  };
  image_url: any;
  generalTranslate: any;
  contact: any;
  MESSAGE_ERROR: any;
  me: any;
  DATA:any;
  constructor(
    public directory: DirectoryProvider,
    public navParams: NavParams,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public statusBar: StatusBar,
    public navCtrl: NavController,
    public platform: Platform,
    public viewCtrl: ViewController,
    public contacts: Contacts,
    public toastCtrl: ToastController,
    public storage: Storage,
    public general: GeneralProvider
  ) {
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
    this.storage.get("user_access").then(data => {
      this.me = data;
    });
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA").subscribe((res: string) => {
      this.generalTranslate = res;
    });
    this.userDirectory.push({
      element: "width",
      value: "140"
    });
    this.userDirectory.push({
      element: "class",
      value: "center-align-content image"
    });

    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    this.getLangMessage();
    this.user = {
      avatar: "example",
      name: "example",
      last_name: "example",
      postion: {
        name: "example"
      },
      company: {
        name: "example"
      }
    };
  }
  ionViewDidLoad(){

  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
        //this.showPrompt();
        console.log(this.viewCtrl.name);
      } else {
        this.navCtrl.pop();
        console.log(this.viewCtrl.name);
      }
    }, 1);
  }

  goToHome() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: "forward" });
  }

  async getLangMessage() {
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
      this.getParams();
    });
  }

  async getParams() {
    try {
      const data: any = await this.navParams.get("user");
      const img_url: any = await this.navParams.get("image_url");
      this.contact = await this.navParams.get("contact");
      this.image_url = img_url;
      this.getUser(data);
    } catch (e) {
      console.log(e);
      e.error.errors.messages.forEach(element => {
        let options = {
          title: "Oops!",
          subTitle: element,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      });
    }
  }
  async getUser(id: any) {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();
    try {
      const data: any = await this.directory.getUserById(id);
      if(data.position){
        this.position = data.position;
      }
      this.company = data.company[0];
      this.user = data;

      //loadingIMG
      this.userDirectory.push({
        element: "onerror",
        value: "this.onerror=null;this.src='" + this.imgDefault + "';"
      });

      this.image_head.push({
        element: "class",
        value: "image-head"
      });

      await this.storage.get("contact_detail_intro_finish").then((data:any)=>{
        if(!data){
          this.intro();
        }
      })

      loading.dismiss();
    } catch (e) {
      this.storage.get("user_access").then(data => {
        if(this.contact.position){
        this.position = this.contact.position;
        }
        this.company = data.company[0];
        this.user = this.contact;

      });

      await this.storage.get("contact_detail_intro_finish").then((data:any)=>{
        if(!data){
          this.intro();
        }
      })

      loading.dismiss();
      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  phone = '0315875333'
  phoneMask = '(571) 587 53 33'

  contactCreate() {

    // if (this.user.name != null && this.user.phone != null) {
    let contact: Contact = this.contacts.create();

    contact.name = new ContactName(null, this.user.last_name, this.user.name);
    contact.phoneNumbers = [new ContactField("work", this.phone)];
    var photos = [];
    var ImgUrl = this.AUTH.API_URL + "/" + this.user.avatar;
    photos[0] = new ContactField("url", ImgUrl, true);
    contact.photos = photos;

    var organizations = [];
    // contact.name = new ContactName(null, '', data.name);
    organizations[0] = new ContactOrganization(
      "name",
      this.company.name,
      "title",
      this.position.name
    );
    // organizations[1] = new ContactOrganization('title', this.position.name);
    contact.organizations = organizations;
    // 'title', this.position.name

    contact.emails = [new ContactField("work", this.user.email)];
    // contact.emails = [new ContactField('email', data.email)];
    contact.note = this.generalTranslate.DIGITAL_CARD.TABS.SEND.MESSAGE_NOTE_CONTACT;

    //contact.save().then(
    //() => console.log('Contact saved!', contact),
    //(error: any) => console.error('Error saving contact.', error)
    //);

    contact
      .save()
      .then(contact => {
        let toast = this.toastCtrl.create({
          message: this.generalTranslate.GENERAL_MESSAGES.OK
            .CONTACT_SAVED_SUCCESSFULLY,
          duration: 2000,
          position: "bottom"
        });
        toast.present(toast);

        console.log("Contact saved!", contact);
      })
      .catch(error => {
        let toast = this.toastCtrl.create({
          message: this.generalTranslate.GENERAL_MESSAGES.ERROR
            .ERROR_SAVED_CONTACT,
          duration: 2000,
          position: "bottom"
        });
        toast.present(toast);
      });
    // } else {
    //   let alert = this.alertCtrl.create({
    //     title: this.generalTranslate.GENERAL_MESSAGES.ERROR.WARNING_TITLE,
    //     subTitle: this.generalTranslate.GENERAL_MESSAGES.ERROR
    //       .WARNING_PHONE_AND_NAME,
    //     buttons: [
    //       {
    //         text: this.generalTranslate.GENERAL_MESSAGES.OK.I_AGREE,
    //         cssClass: "alerCtrlBtnPrimary"
    //       }
    //     ]
    //   });
    //   alert.present();
    // }
  }
  intro() {
    let me = this;
    let intro = introJs();
    intro.setOptions({
    steps: [
      {
        element: '#btn-save_phone',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.DIRECTORY.BTN_SAVE_CONTACT+"</span>",
        position: 'bottom'
      }
    ],
    showBullets: false,
    showStepNumbers: false,
    hidePrev: true,
    hideNext: true,
    nextLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.NEXT+"</span>",
    prevLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.PREVIOUS+"</span>",
    skipLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.SKIP+"</span>",
    doneLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.DONE+"</span>"
    });
    intro.oncomplete(function(){
      me.storage.set("contact_detail_intro_finish", true);
    });
    intro.onexit(function() {
      me.storage.set("contact_detail_intro_finish", true);
    });
    intro.start();
  }
}
