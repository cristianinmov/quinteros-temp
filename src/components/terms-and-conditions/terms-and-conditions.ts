import { Component } from "@angular/core";
import { ViewController, Platform, LoadingController } from "ionic-angular";
import { GeneralProvider } from "../../providers/general/general";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { NavParams } from "ionic-angular";

/**
 * Generated class for the TermsAndConditionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "terms-and-conditions",
  templateUrl: "terms-and-conditions.html"
})
export class TermsAndConditionsComponent {
  MESSAGE_ERROR: any;
  lang: any;
  info: any = {
    terms_es: "",
    policy_es: "",
    terms_en: "",
    policy_en: ""
  };
  wait: any;
  status:any;
  constructor(
    public viewCtrl: ViewController,
    public platform: Platform,
    public general: GeneralProvider,
    public storage: Storage,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public navParams : NavParams
  ) {}
  closeApp() {
    this.platform.exitApp();
  }

  async ionViewDidEnter() {
    this.status = await this.navParams.get("status");
    await this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();

    await this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });

    await this.translate.get("LOCAL").subscribe(value => {
      this.lang = value;
    });

    try {
      const response: any = await this.general.getTermsAndConditions();
      this.info = response[0];
      loading.dismiss();
    } catch (e) {
      loading.dismiss();
      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            };
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              };
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          };
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async agree(){
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    try{
      loading.present();
      const response : any = await this.general.agreeTermsAndConditions()
      console.log(response);
      loading.dismiss();
      this.dismiss();
    }
    catch(e){
      loading.dismiss();
      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            };
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              };
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          };
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
}
