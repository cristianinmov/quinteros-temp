import { Component } from '@angular/core';
import { ViewController, Platform } from 'ionic-angular';
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Storage } from "@ionic/storage";


/**
 * Generated class for the AppPermissionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-permission',
  templateUrl: 'app-permission.html'
})
export class AppPermissionComponent {

  text: string;

  constructor(public viewCtrl: ViewController,
    public androidPermissions: AndroidPermissions,
    public platform: Platform,
    public storage: Storage
    ) {
    console.log('Hello AppPermissionComponent Component');
    this.text = 'Hello World';


  }
  goToPermissions(){
    // console.log('accede a la función');
    if (this.platform.is('android')) {
      // platform.ready().then(() => {
        // androidPermissions.requestPermissions(
        //   [
        //     androidPermissions.PERMISSION.CAMERA,
        //     // androidPermissions.PERMISSION.CALL_PHONE,
        //     androidPermissions.PERMISSION.READ_CONTACTS,
        //     androidPermissions.PERMISSION.WRITE_CONTACTS,
        //     androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
        //     androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
        //     // androidPermissions.PERMISSION.GET_ACCOUNTS,
        //     androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
        //     androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
        //   ]
        // );

        // Permisos
        // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
        //   result => console.log('Has permission?', result.hasPermission),
        //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
        // );

        // this.androidPermissions.checkPermission().then(
        //   result => console.log('Has permission contacts?', result.hasPermission),
        //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_CONTACTS)
        // );

        this.androidPermissions.requestPermissions([
          this.androidPermissions.PERMISSION.CAMERA,
          this.androidPermissions.PERMISSION.READ_CONTACTS,
          this.androidPermissions.PERMISSION.WRITE_CONTACTS,
          this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
          this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
          this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
          this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
        ]);

        this.androidPermissions.checkPermission(
          this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION &&
          this.androidPermissions.PERMISSION.READ_CONTACTS &&
          this.androidPermissions.PERMISSION.CAMERA &&
          this.androidPermissions.PERMISSION.WRITE_CONTACTS &&
          this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION &&
          this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE &&
          this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          ).then(
          result => console.log('Has general permission?', result.hasPermission),
          err => this.androidPermissions.requestPermission(
            this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION &&
            this.androidPermissions.PERMISSION.READ_CONTACTS &&
            this.androidPermissions.PERMISSION.CAMERA &&
            this.androidPermissions.PERMISSION.WRITE_CONTACTS &&
            this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION &&
            this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE &&
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          )
        );

        this.viewCtrl.dismiss();
        this.storage.set("permissions_modal", {modalDisable: true});

  // })
    }
  }
}
