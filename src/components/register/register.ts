import { LoginPage } from './../../pages/login/login';
import { environment } from "./../../environments/environment";
import { StatusBar } from "@ionic-native/status-bar";
import {
  ViewController,
  NavController,
  AlertController,
  LoadingController,
  ToastController,
  ModalController
} from "ionic-angular";
import { Component } from "@angular/core";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { GooglePlus } from "@ionic-native/google-plus";
import { Device } from "@ionic-native/device";
import { AuthProvider } from "../../providers/auth/auth";
import { HomePage } from "../../pages/home/home";
import { Globalization } from "@ionic-native/globalization";

//Form Validation
import { Validators, FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { GeneralProvider } from '../../providers/general/general';
import { TermsAndConditionsComponent } from '../terms-and-conditions/terms-and-conditions';

/**
 * Generated class for the RegisterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "register",
  templateUrl: "register.html"
})
export class RegisterComponent {
  text: string;
  MESSAGE_ERROR: any;
  wait: any;
  LOCAL_STORAGE = environment.LOCAL_STORAGE;
  AUTH = environment.API;
  LOCAL_LANG: any;
  DATA: any;
  message: any;
  type_register:any = "M";
  agree:boolean = false;
  form:any = {
    email: "",
    password: "",
    first_name: "",
    last_name: "",
    company: ""
  }
  email_val:any;
  first_name_val:any;
  last_name_val:any;
  lang:any;
  validation_messages:any;
  company:any;
  password:any;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  public formRegister: FormGroup;

  constructor(
    public navCtrl2: ViewController,
    public navCtrl: NavController,
    public statusBar: StatusBar,
    public storage: Storage,
    public navController: NavController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public googlePlus: GooglePlus,
    public device: Device,
    public auth: AuthProvider,
    public toastCtrl: ToastController,
    public globalization: Globalization,
    public formBuilder: FormBuilder,
    public general: GeneralProvider,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController
  ) {
    this.getLang();

    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    this.translate.get("LOCAL").subscribe(value => {
      this.LOCAL_LANG = value;
    });
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
    this.translate.get("DATA.LOGIN.HI").subscribe(value => {
      this.message = value;
    });
    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });

    //FORMVALIDATIOS
    this.validation_messages = this.DATA.VALIDATIONS;
    if(this.type_register == "M"){
      this.formRegister = this.formBuilder.group({

        email: ["",Validators.compose(
          [ Validators.maxLength(70),
            Validators.pattern("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"),
            Validators.required
          ])
        ],
        first_name: new FormControl('', Validators.required),
        last_name: new FormControl('', Validators.required),
        company: new FormControl('', Validators.required),
        password: new FormControl('',  Validators.compose([
          Validators.required,
          Validators.minLength(6)
        ])),
        agree: new FormControl(false, Validators.requiredTrue)
      });
    }
  }

  ionViewDidLoad(){}

  async getLang(){
    const data: any = await this.globalization.getLocaleName();
    /* this.lang =  data.value.charAt(0) + data.value.charAt(1); */
    this.lang = 'es';
  }

  goToBack() {
    this.navCtrl2.dismiss();
  }
  goToHome() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: "forward" });
  }

  async saveUser() {
    if(!this.formRegister.valid){
      Object.keys(this.formRegister.controls).forEach(field => {
        const control = this.formRegister.get(field);
        control.markAsTouched({ onlySelf: true });
      });
      return;
    }
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();
    let obj: any = {
      email: this.formRegister.controls.email.value,
      password: this.formRegister.controls.password.value,
      first_name: this.formRegister.controls.first_name.value,
      last_name: this.formRegister.controls.last_name.value,
      company: this.formRegister.controls.company.value,
      type_register: this.type_register,
      tyc: true,
      language: (this.lang) ? this.lang : "es"
    };
    try {
      const data: any = await this.auth.register(obj);
      data.errors.messages.forEach(element => {
        let options = {
          title: "Ok",
          subTitle: element,
          buttons: [{
            text: 'OK',
            role: 'cancel',
            handler: () => {
              this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
            }
          },],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);

      });
      loading.dismiss();
    } catch (e) {
      loading.dismiss();
      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });
          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  

  aadLogin() {
    this.resetProccess();
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();

    this.auth.login365()
      .then((res: any) => {
        this.formRegister = this.formBuilder.group({
          email: ["",Validators.compose(
            [ Validators.maxLength(70),
              Validators.pattern("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"),
              Validators.required
            ])
          ],
          first_name: new FormControl('', Validators.required),
          last_name: new FormControl('', Validators.required),
          company: new FormControl('', Validators.required),
          password: new FormControl(''),
          agree: new FormControl(false, Validators.requiredTrue)
        });

        this.formRegister.value.email = res.userInfo.uniqueId;
        this.formRegister.value.first_name = res.userInfo.givenName;
        this.formRegister.value.last_name = res.userInfo.familyName;

        this.email_val = res.userInfo.uniqueId;
        this.first_name_val = res.userInfo.givenName;
        this.last_name_val  = res.userInfo.familyName;
        this.password = null;
        this.type_register = "S";
        loading.dismiss()
      })
      .catch(err => {
        const showErrors = [7, 17, 10, 13, 8, 14, 5, 3, 4, -1, 15, 16];
        if (showErrors.indexOf(err) > -1) {
          let options = {
            title: "Oops!",
            subTitle: this.translate.instant(`DATA.GOOGLE_LOGIN_ERRORS.${err}`),
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
        }

        loading.dismiss();
      });
  }
  onKey(){
    this.email_val = this.email_val.replace(/\s/g, '');
  }
  resetProccess(){
    this.email_val = '';
    this.first_name_val = '';
    this.last_name_val  = '';
    this.type_register = "M";
    this.password = '';
    this.company = '';
  }
  goToLink(){
    const terms = this.modalCtrl.create(TermsAndConditionsComponent);
    terms.present();
  }
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
}
