import { Storage } from "@ionic/storage";
import { StatusBar } from "@ionic-native/status-bar";
import { SendComponent } from "./../digital-card-send/send";
import { Component } from "@angular/core";
import {
  Platform,
  ActionSheetController,
  ToastController,
  LoadingController,
  NavController,
  AlertController,
  ViewController
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";

import { Clipboard } from "@ionic-native/clipboard";
import { SocialSharing } from "@ionic-native/social-sharing";
// List Service
import { DigitalCardProvider } from "../../providers/digital-card/digital-card";
import { LoginPage } from "./../../pages/login/login";

import { environment } from "../../environments/environment";

import { GeneralProvider } from "../../providers/general/general";

import { Network } from "@ionic-native/network";

// IntroJs
import introJs from '../../../node_modules/intro.js/intro';

@Component({
  selector: "saved",
  templateUrl: "saved.html"
})
export class SavedComponent {
  API = environment.API;

  root: SendComponent;
  text: string;
  dataEnd: any = [];
  cellphone: any;
  generalTranslate: any;
  showNotContacts: boolean;
  pageInit: any = 1;
  publications: any;
  wait: any;
  MESSAGE_ERROR;
  publicationsArray: any;
  DATA:any;

  constructor(
    public toastCtrl: ToastController,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    public storage: Storage,
    public statusBar: StatusBar,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    public translate: TranslateService,
    public digitalCardProvider: DigitalCardProvider,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public general: GeneralProvider,
    public network: Network,
    public alertCtrl: AlertController
  ) {
    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    // var generalTranslate;
    this.translate.get("DATA").subscribe((res: string) => {
      this.generalTranslate = res;
    });

    this.wait = this.generalTranslate.WAIT;
    this.localStorageList();
  }

  ionViewDidEnter() {
    this.getAllContacts();
  }

  moreOptions(item) {
    var options;
    console.log(item)
    this.translate
      .get("DATA.DIGITAL_CARD.TABS.SAVED")
      .subscribe((res: string) => {
        options = res;
      });

    let optionMail = {
      text: options.MORE_OPTIONS_SEVED.SEND_EMAIL,
      icon: !this.platform.is("ios") ? "mail" : null,
      handler: () => {
        this.storage
          .get("user_access")
          .then(val => {
            this.socialSharing
              .shareViaEmail(
                "Hola, te comparto mi tarjeta de contacto " +
                  val.card_url +
                  ", atte. " +
                  val.name +
                  "",
                "Los Quintero App",
                item.mail
              )
              .then(() => {});

            // Success!
          })
          .catch(() => {
            let toast = this.toastCtrl.create({
              message: this.generalTranslate.GENERAL_MESSAGES.ERROR_MAIL,
              duration: 2000,
              position: "bottom"
            });
            toast.present(toast);
          });
        console.log("Mail clicked");
      }
    };

    let actionSheet = this.actionsheetCtrl.create({
      title: options.MORE_OPTIONS_SEVED.MESSAGE_TITLE,
      cssClass: "action-sheets-basic-page",
      buttons: [
        {
          text: options.MORE_OPTIONS_SEVED.RESEND_CARD,
          icon: !this.platform.is("ios") ? "send" : null,
          handler: () => {
            this.resendCardOptions(item.country_phone, item.phone);
          }
        },
        {
          text: options.MORE_OPTIONS_SEVED.COPY_NUMBER,
          icon: !this.platform.is("ios") ? "copy" : null,
          handler: () => {
            this.clipboard.copy(item.country_phone + item.phone);
            let toast = this.toastCtrl.create({
              message: this.generalTranslate.DIGITAL_CARD.TABS.SEND.MESSAGES
                .COPIED,
              duration: 2000,
              position: "bottom"
            });
            toast.present(toast);
          }
        },
        {
          text: options.MORE_OPTIONS_SEVED.CANCEL,
          role: "cancel", // will always sort to be on the bottom
          icon: !this.platform.is("ios") ? "close" : null,
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    // if(!item.mail){
    //   actionSheet.destructiveText = 'Delete';
    // }
    if (item.email != null) {
      actionSheet.addButton(optionMail);
    }
    actionSheet.present();
  }

  // Posibility Obervable
  resendCardOptions(prefix, cellphone) {
    var options;
    this.translate
      .get("DATA.DIGITAL_CARD.TABS.SEND")
      .subscribe((res: string) => {
        options = res;
      });

    let actionSheet = this.actionsheetCtrl.create({
      title: options.MORE_OPTIONS_SEND.SEND_VIA,
      cssClass: "action-sheets-basic-page",
      buttons: [
        {
          text: options.MORE_OPTIONS_SEND.SMS,
          role: "destructive",
          icon: !this.platform.is("ios") ? "text" : null,
          handler: () => {
            this.storage.get("user_access").then(val => {
              this.socialSharing
                .shareViaSMS(
                  "Hola, te comparto mi tarjeta de contacto " +
                    val.card_url +
                    ", atte. " +
                    val.name +
                    "",
                  prefix + cellphone
                )
                .then(() => {
                  // Success!
                })
                .catch(() => {
                  let toast = this.toastCtrl.create({
                    message: this.generalTranslate.GENERAL_MESSAGES.ERROR_SMS,
                    duration: 2000,
                    position: "bottom"
                  });
                  toast.present(toast);
                });
            });
          }
        },
        {
          text: options.MORE_OPTIONS_SEND.WHATSAPP,
          icon: !this.platform.is("ios") ? "logo-whatsapp" : null,
          handler: () => {
            // if()
            this.storage.get("user_access").then(val => {
              // Share via email
              // if (this.send.phone) {
              this.socialSharing
                .shareViaWhatsAppToReceiver(
                  prefix + cellphone,
                  "Hola, te comparto mi tarjeta de contacto " +
                    val.card_url +
                    ", atte. " +
                    val.name +
                    ""
                )
                .then(() => {
                  // Success!
                })
                .catch(() => {
                  let toast = this.toastCtrl.create({
                    message: this.generalTranslate.GENERAL_MESSAGES
                      .ERROR_WHATSAPP,
                    duration: 2000,
                    position: "bottom"
                  });
                  toast.present(toast);
                });
            });
          }
        },
        {
          text: options.MORE_OPTIONS_SEND.MY_APPS,
          icon: !this.platform.is("ios") ? "apps" : null,
          handler: () => {
            this.storage.get("user_access").then(val => {
              // Share via email
              this.socialSharing
                .share(
                  "Hola, te comparto mi tarjeta de contacto " +
                    val.card_url +
                    ", atte. " +
                    val.name +
                    "",
                  "Los Quintero App"
                )
                .then(() => {
                  // Success!
                })
                .catch(() => {
                  let toast = this.toastCtrl.create({
                    message: this.generalTranslate.GENERAL_MESSAGES.ERROR_APPS,
                    duration: 2000,
                    position: "bottom"
                  });
                  toast.present(toast);
                });
            });

            console.log("My apps clicked");
          }
        },
        {
          text: options.MORE_OPTIONS_SEND.CANCEL,
          role: "cancel", // will always sort to be on the bottom
          icon: !this.platform.is("ios") ? "close" : null,
          cssClass: "color-cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  async getAllContacts() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();
    try {
      const response: any = await this.digitalCardProvider.getAllContacts(1);
      if (response.data.length > 0) {
        this.publications = response.data;
      }
      loading.dismiss();
      if (response.data.length == 0) {
        this.showNotContacts = true;
        // this.publications = [
        //   {
        //     id: 54,
        //     it_business_id: 1,
        //     phone: "3010000000",
        //     country_phone: "+57",
        //     name: "example",
        //     email: "example@examplecom",
        //     position: "example",
        //     business: "example",
        //     registre_time: "2000-01-01 13:47:03",
        //     latitude: "4.730402",
        //     longitude: "-74.024125",
        //     placeid: null,
        //     point: null,
        //     it_users_id: 13,
        //     created_at: "2000-01-01 13:47:03",
        //     updated_at: "2000-01-01 13:47:03",
        //     deleted_at: null
        //   }];
      }
      //firstTime
      this.translate.get("DATA").subscribe(value => {
        this.DATA = value;
      });
      this.storage.get("mdc_saved_intro_finish").then((data:any)=>{
        if(!data){
          this.intro(response.data);
        }
      })
    } catch (e) {
      loading.dismiss();

      await this.network.onDisconnect().subscribe(() => {
        this.localStorageList();
      });
    }
  }

  async localStorageList() {
    await this.storage.get("MDC_SAVED").then(val => {
      var obj_saved = val;
      this.publicationsArray = [];
      this.storage.get("user_access").then(value => {
        if (obj_saved != null) {
          this.showNotContacts = false;
          val.forEach((item, index) => {
            if (obj_saved[index].it_users_id == value.id) {
              this.publicationsArray.push(obj_saved[index]);
            }
          });
          this.publications = this.publicationsArray;
        } else {
          this.showNotContacts = true;
        }
      });
    });
  }

  async doRefresh(refresher) {
    try {
      const response: any = await this.digitalCardProvider.getAllContacts(1);
      this.publications = response.data;
      refresher.complete();
      this.pageInit = 1;
    } catch (e) {
      refresher.complete();

      console.log("accede al doRefresh");
      await this.localStorageList();

      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);

          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
            this.MESSAGE_ERROR = value;
          });
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
        //catch in te interceptor
      }
    }
  }

  async doInfinite(infiniteScroll) {
    this.pageInit++;
    try {
      const response: any = await this.digitalCardProvider.getAllContacts(
        this.pageInit
      );
      response.data.forEach(element => {
        this.publications.push(element);
      });

      infiniteScroll.close();
    } catch (e) {}
  }
  intro(array) {
    let arrayPrev = array;
    let me = this;
    let intro = introJs();
    intro.setOptions({
    steps: [
      {
        element: '#call_phone',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.MDC.SAVED.CALL+"</span>",
        position: 'bottom'
      },
      {
        element: '#more_options_contact',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.MDC.SAVED.MORE_OPTIONS+"</span>",
        position: 'bottom'
      }
    ],
    showBullets: false,
    showStepNumbers: false,
    hidePrev: true,
    hideNext: true,
    nextLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.NEXT+"</span>",
    prevLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.PREVIOUS+"</span>",
    skipLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.SKIP+"</span>",
    doneLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.DONE+"</span>"
    });
    intro.oncomplete(function(){
      me.storage.set("mdc_saved_intro_finish", true);
      me.publications = arrayPrev;
    });
    intro.onexit(function() {
      me.storage.set("mdc_saved_intro_finish", true);
    });
    intro.onexit(function(){
      me.publications = arrayPrev;
    });
    // intro.start();
  }
}
