import { environment } from "./../../environments/environment";
import { StatusBar } from "@ionic-native/status-bar";
import { Component, ElementRef, ViewChild } from "@angular/core";
import { CommutyProvider } from "../../providers/commuty/commuty";
import {
  Platform,
  ActionSheetController,
  LoadingController,
  NavController,
  AlertController,
  NavParams,
  ViewController,
  normalizeURL
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";

import { Camera, CameraOptions } from "@ionic-native/camera";
import { Storage } from "@ionic/storage";
import { LoginPage } from "../../pages/login/login";
import { File, FileEntry } from "@ionic-native/file";
import { FileService } from "../../pages/profile/file.service";

import { Base64 } from '@ionic-native/base64';
import { DomSanitizer } from "@angular/platform-browser";
import { GeneralProvider } from "../../providers/general/general";
import { HomePage } from "../../pages/home/home";

@Component({
  selector: "community-new-publication",
  templateUrl: "community-new-publication.html"
})
export class CommunityNewPublicationComponent {
  @ViewChild("myInput") myInput: ElementRef;

  option;
  text: string;
  user_access: any = {
    can_notify: "N"
  }
  wait;
  MESSAGE_ERROR;
  callback;
  base64Image: any;
  myImage: any = "";
  api: any = environment.API;
  myImageFile: any;
  imagePath: any;
  loader: any;
  notify_all:number = 0;
  DATA:any;

  constructor(
    public statusBar: StatusBar,
    public commutyProvider: CommutyProvider,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    public translate: TranslateService,
    public camera: Camera,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    public navController: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public file: File,
    public fileService: FileService,
    public base64: Base64,
    public general: GeneralProvider,
    public _DomSanitizationService: DomSanitizer
  ) {
    this.callback = this.navParams.get("callback");

    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
    this.translate.get("DATA.OPTION").subscribe(value => {
      this.option = value;
    });
    this.storage
      .get("user_access")
      .then((response: any) => {
        this.user_access = response;
      })
      .catch((err: any) => {
        console.log(err);
      });
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
   //ConfigStatusBar
   this.statusBar.backgroundColorByHexString("#E76114");
   this.statusBar.styleLightContent();
  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
      } else {
        this.navCtrl.pop();
      }
    }, 1);
  }

  async saveNewPublication() {
    try {
      let data: any = {};
      await this.commutyProvider.saveNewPublication(data);
    } catch (e) {
      console.log(e);
    }
  }

  openSelectImage() {
    let actionSheet = this.actionsheetCtrl.create({
      title: this.option,
      cssClass: "action-sheets-basic-page",
      buttons: [
        {
          text: "Take photo",
          role: "destructive",
          icon: !this.platform.is("ios") ? "ios-camera-outline" : null,
          handler: () => {
            //this.captureImage(false);
            this.captureImagePreview(false);
          }
        },
        {
          text: "Choose photo from Gallery",
          icon: !this.platform.is("ios") ? "ios-images-outline" : null,
          handler: () => {
            //this.captureImage(true);
            this.captureImagePreview(true);
          }
        }
      ]
    });
    actionSheet.present();
  }

  async captureImagePreview(useAlbum: boolean) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true
    };
    options.sourceType = useAlbum
      ? this.camera.PictureSourceType.PHOTOLIBRARY
      : this.camera.PictureSourceType.CAMERA;
    try {
      var imageData:any = await this.camera.getPicture(options);
      this.myImageFile = imageData;
      if(this.platform.is('ios')){
        imageData = await normalizeURL(imageData);
      }
      this.base64.encodeFile(imageData).then((imgEnd64: string)=>{
        this.myImage = imgEnd64;
      }).catch((err)=>{
        console.log(err);
      })
      //this.uploadFile();
    } catch (e) {
      console.log(e);
    }
  }
  async uploadFile() {
    this.loader = this.loadingCtrl.create({
      content: this.DATA.COMMUNITY.COMPONENTS.LOADING
    });
    this.loader.present();

    try {
      if(this.myImageFile){
        this.file
        .resolveLocalFilesystemUrl(this.myImageFile)
        .then(entry => {
          (<FileEntry>entry).file(file => this.readFile(file));
        })
        .catch(err => console.log(err));
      }
      else{
        this.loader.dismiss();
        this.postOublication();
      }
    } catch (e) {
      console.log(e);
      this.loader.dismiss();
    }
  }

  readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      formData.append("file", imgBlob, file.name);
      this.sendImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }

  async sendImageData(formData: FormData) {
    this.fileService.uploadImg(formData).subscribe(result => {
      this.imagePath = result.path;
      this.loader.dismiss();
      this.postOublication();
    });
  }

  async captureImage(useAlbum: boolean) {
    const options: CameraOptions = {
      quality: 50,
      targetHeight: 800,
      targetWidth: 800,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true
    };
    options.sourceType = useAlbum
      ? this.camera.PictureSourceType.PHOTOLIBRARY
      : this.camera.PictureSourceType.CAMERA;
    try {
      // const imageData = await this.camera.getPicture(options);
      // this.imageURI = imageData;
      // this.uploadFile();
    } catch (e) {
      console.log(e);
    }
  }

  async postOublication() {
    //MessageToast
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();
    try {
      let obj = {
        it_business_id: this.user_access.company[0].id,
        text: this.text,
        it_users_id: this.user_access.id,
        status: "A",
        photo: this.imagePath,
        notify_all: (this.notify_all) ? 1 : 0
      };
      await this.commutyProvider.saveNewPublication(obj);
      this.callback(obj).then(() => {
        this.navController.pop();
      });
      loading.dismiss();

    } catch (e) {
      loading.dismiss();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navController.setRoot(LoginPage);

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
        //catch in te interceptor
      }
    }
  }

  resize() {
    var element = this.myInput[
      "_elementRef"
    ].nativeElement.getElementsByClassName("text-input")[0];
    var scrollHeight = element.scrollHeight;
    element.style.height = scrollHeight + "px";
    this.myInput["_elementRef"].nativeElement.style.height =
      scrollHeight + 16 + "px";
  }
  deleteSelectImage(){
    this.myImage = "";
  }
  goToHome(){
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: "forward" });
  }
}
