import { GeneralProvider } from './../../providers/general/general';
import { Component, ElementRef, ViewChild } from "@angular/core";
import { BlogProvider } from "../../providers/blog/blog";
import {
  NavParams,
  NavController,
  AlertController,
  LoadingController,
  Platform,
  ViewController,
  Content
} from "ionic-angular";
import { CommutyProvider } from "../../providers/commuty/commuty";
import { LoginPage } from "../../pages/login/login";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../environments/environment";

@Component({
  selector: "commentary",
  templateUrl: "commentary.html"
})
export class CommentaryComponent {
  @ViewChild("myInput") myInput: ElementRef;
  @ViewChild(Content) content: Content;
  @ViewChild("messageArea") messageArea: Content;
  API = environment.API;
  DEFAULT_IMG = environment.USER_DEFAULT;
  comments: any = false;
  wait;
  MESSAGE_ERROR;
  message_text: any;
  commetnary: any;
  me_user: any;
  index_of_community: any;
  callback: any;

  constructor(
    public blogProvider: BlogProvider,
    public navParams: NavParams,
    public commutyProvider: CommutyProvider,
    public storage: Storage,
    public navController: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public translate: TranslateService,
    public platform: Platform,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public general: GeneralProvider
  ) {
    this.storage.get("user_access").then(data => {
      this.me_user = data;
      console.log(this.me_user);
    });
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.getParams();
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
        //this.showPrompt();
        console.log(this.viewCtrl.name);
      } else {
        this.navCtrl.pop();
        console.log(this.viewCtrl.name);
      }
    }, 1);
  }

  async getParams() {
    try {
      const commetnary: any = await this.navParams.get("community");
      this.index_of_community = await this.navParams.get("index");
      this.callback = this.navParams.get("callback");
      this.commetnary = commetnary;
      this.getAllcomentaries(commetnary);
    } catch (e) {
      console.log(e);
    }
  }

  async getAllcomentaries(data) {
    //MessageToast
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();
    try {
      const response: any = await this.commutyProvider.getAllComments(data.id);

      if (response.data.length > 0) {
        setTimeout(function()
        {var objDiv = document.getElementById("messageArea");
          objDiv.scrollTop = objDiv.scrollHeight;
        }, 1);
        this.comments = response.data;
      } else {
        this.comments = false;
      }
      loading.dismiss();
    } catch (e) {
      loading.dismiss();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navController.setRoot(LoginPage);

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  async saveComment() {
    //MessageToast
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();
    let obj = {
      it_users_id: this.me_user.id,
      comment: this.message_text
    };
    try {
      await this.commutyProvider.saveComment(this.commetnary.id, obj);
      this.getAllcomentaries(this.commetnary);
      this.message_text = "";
      //this.comments = "";
      loading.dismiss();
      //this.CommunityPage.publications
      this.callback(this.index_of_community);
    } catch (e) {
      loading.dismiss();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navController.setRoot(LoginPage);

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        //catch in te interceptor
      }
    }
  }

  resize() {
    var element = this.myInput[
      "_elementRef"
    ].nativeElement.getElementsByClassName("text-input")[0];

    var scrollHeight = element.scrollHeight;

    element.style.height = scrollHeight + "px";

    this.myInput["_elementRef"].nativeElement.style.height =
      scrollHeight + 16 + "px";
  }

  myfunction() {
    this.content.scrollTo(0, 200);
  }
  checkBlur() {
    this.myInput["_elementRef"].nativeElement.style.paddingBottom = "0px";
    this.content.scrollToBottom();
  }
  checkFocus() {
    // this.myInput["_elementRef"].nativeElement.style.paddingBottom = "329px";
    // this.content.scrollToBottom();
  }
}
