import { GeneralProvider } from './../../providers/general/general';
import { Component } from "@angular/core";
import {
  ViewController,
  AlertController,
  LoadingController,
  ToastController,
  NavParams
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { TranslateService } from "@ngx-translate/core";
import { Clipboard } from '@ionic-native/clipboard';
import { Validators, FormBuilder, FormGroup } from "@angular/forms";

/**
 * Generated class for the ForgotPaswordComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "forgot-pasword",
  templateUrl: "forgot-pasword.html"
})
export class ForgotPaswordComponent {
  text: string;
  email: any = "";
  code: any = "";
  MESSAGE_ERROR: any;
  wait: any;
  step: number = 1;
  data:any;
  callback:any;
  type_method:any = "email";
  selectOptions:any = {
    cssClass: "alertErrors",
    mode: 'md'
  }
  validation_messages:any;
  public myGroup: FormGroup;

  constructor(
    public viewCtrl: ViewController,
    public auth: AuthProvider,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    private clipboard: Clipboard,
    public formBuilder: FormBuilder,
    public general: GeneralProvider
  ) {
    this.translate.get("DATA").subscribe(value => {
      this.data = value;
    });
    this.validation_messages = this.data.VALIDATIONS;
    this.myGroup = this.formBuilder.group({
      email: [
        "",
        Validators.compose([
          Validators.maxLength(70),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ]
    });
    this.callback = this.navParams.get("callback");
    if(this.navParams.get("email")){
      this.email = this.navParams.get("email");
    }
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    this.translate.get("DATA").subscribe(value => {
      this.data = value;
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  async sendEmail() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    let email = {
      item: this.email,
      method: this.type_method
    };
    try {
      const response: any = await this.auth.forgotPasswordSendEmail(email);
      // this.email = "";
      this.step = 2;
      response.errors.messages.forEach(element => {
        let toast = this.toastCtrl.create({
          message: element,
          duration: 3000,
          position: "button"
        });
        toast.present(toast);
      });
      loading.dismiss();
    } catch (e) {
      loading.dismiss();

      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
        //catch in te interceptor
      }
    }
  }

  resetForm() {
    this.step = 1;
    this.email = "";
    this.code = "";
    this.type_method = "email";
  }
  async Validate() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    let code = {
      item: this.email,
      token: this.code
    };
    try {
      const response: any = await this.auth.validateCodeForgotPassword(code);
      loading.dismiss();

      let options = {
        title: this.data.FORGOT_PASSWORD.PASSWORD_GENERATE,
        subTitle: this.data.FORGOT_PASSWORD.MESSAGE_TEMP + response.response.messages[0],
        buttons: [
          {
            text: this.data.FORGOT_PASSWORD.BTM_CANCEL,
            role: "cancel",
            handler: data => {
              console.log("Cancel clicked");
            }
          },
          {
            text: this.data.FORGOT_PASSWORD.BTN_USE,
            handler: data => {
              this.callback({
                email: this.email,
                password: response.response.messages[0]
              }).then(()=>{
                this.viewCtrl.dismiss();
              })
            }
          },
          {
            text: this.data.FORGOT_PASSWORD.COPY,
            handler: data => {
             this.clipboard.copy(response.response.messages[0]).then(()=>{
               let toast = this.toastCtrl.create({
                message: this.data.DIGITAL_CARD.TABS.SEND.MESSAGES.COPIED,
                duration: 2000,
                position: "button"
              });
              toast.present(toast);
             })
            }
          }
        ],
        cssClass: "alertErrors"
      }
      this.general.presentAlertError(options, this.viewCtrl.name);
    } catch (e) {
      loading.dismiss();

      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  onKey(){
    this.email = this.email.replace(/\s/g, '');
  }
}
