import { IonicSelectableModule } from 'ionic-selectable';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SendComponent } from './digital-card-send/send';
import { SavedComponent } from './digital-card-saved/saved';
import { RegisterComponent } from './register/register';
import { CommunityNewPublicationComponent } from './community-new-publication/community-new-publication';
import { BlogDetailComponent } from './blog-detail/blog-detail';
import { BlogCategoryComponent } from './blog-category/blog-category';
import { DirectoryContactComponent } from './directory-contact/directory-contact';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommentaryComponent } from './commentary/commentary';
import { ForgotPaswordComponent } from './forgot-pasword/forgot-pasword';
import { AppPermissionComponent } from './app-permission/app-permission';
import { CommunityDetailComponent } from './community-detail/community-detail';
import { DirectoryBranchComponent } from './directory-branch/directory-branch';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions';
import { BlokedUsersComponent } from './bloked-users/bloked-users';
import { IonicModule } from 'ionic-angular';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
	declarations: [SendComponent,
    SavedComponent,
    RegisterComponent,
    CommunityNewPublicationComponent,
    BlogDetailComponent,
    BlogCategoryComponent,
    DirectoryContactComponent,
    CommentaryComponent,
    ForgotPaswordComponent,
    AppPermissionComponent,
    CommunityDetailComponent,
    DirectoryBranchComponent,
    TermsAndConditionsComponent,
    BlokedUsersComponent],
	imports: [
    IonicSelectableModule,
    IonicModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
  ],
	exports: [SendComponent,
    SavedComponent,
    RegisterComponent,
    CommunityNewPublicationComponent,
    BlogDetailComponent,
    BlogCategoryComponent,
    DirectoryContactComponent,
    CommentaryComponent,
    ForgotPaswordComponent,
    AppPermissionComponent,
    CommunityDetailComponent,
    DirectoryBranchComponent,
    TermsAndConditionsComponent,
    BlokedUsersComponent]
})
export class ComponentsModule {}
