import { GeneralProvider } from './../../providers/general/general';
import { LoginPage } from './../../pages/login/login';
import { HomePage } from "./../../pages/home/home";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { StatusBar } from "@ionic-native/status-bar";
import { Component } from "@angular/core";
import { BlogProvider } from "../../providers/blog/blog";
import {
  NavParams,
  AlertController,
  NavController,
  Platform,
  ViewController,
  LoadingController
} from "ionic-angular";
import { environment } from "../../environments/environment";
import { Storage } from "@ionic/storage";
import { TranslateService } from '@ngx-translate/core';

import { ModalController } from 'ionic-angular';
import { GalleryModal } from 'ionic-gallery-modal';

@Component({
  selector: "blog-detail",
  templateUrl: "blog-detail.html"
})
export class BlogDetailComponent {
  AUTH: any = environment.API;
  news: any;
  BLOG_DEFAULT: any = environment.BLOG_DEFAULT;
  blogAtributes: any[] = [];

  text: string;
  item: any = {
    title: "",
    newscategory: {
      created_at: ""
    }
  };
  me: any;
  callback: any;
  index: any;
  blogGetForParams:any;
  MESSAGE_ERROR:any;
  wait:any;
  infoToNotification:any;

  constructor(
    public statusBar: StatusBar,
    public blogProvider: BlogProvider,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public photoViewer: PhotoViewer,
    public storage: Storage,
    public navCtrl: NavController,
    public platform: Platform,
    public viewCtrl: ViewController,
    public translate : TranslateService,
    public loadingCtrl: LoadingController,
    public general: GeneralProvider,
    public modalCtrl : ModalController
  ) {
    this.item = this.navParams.get("blog");
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    //loadingIMG
    this.blogAtributes.push({
      element: "onerror",
      value: "this.onerror=null;this.src='" + this.BLOG_DEFAULT + "';"
    });
    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    this.getParams();

    this.storage.get("user_access").then(
      (data: any) => {
        this.me = data;
      },
      (errr: any) => {
        console.error(errr);
      }
    );
  }

  includeUserToSource(text) {
    if (!this.me) {
      return '';
    }
    const linkRegex = /((http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?\/?\\?)/g;
    document.getElementById('embed').innerHTML = text.replace(linkRegex, `$1?id=${this.me.id}`)
  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
        console.log(this.viewCtrl.name);
      } else {
        this.navCtrl.pop();
      }
    }, 1);
  }

  async getParams() {
    try {
      const id: any = await this.navParams.get("new");
      this.callback = this.navParams.get("callback");
      this.index = this.navParams.get("index");
      this.blogGetForParams = this.navParams.get("blog");
      this.infoToNotification = this.navParams.get("infoToNotification");
      if(this.infoToNotification){
        this.getNewById(this.infoToNotification.additionalData.item_id);
      }
      if(id){
        this.getNewById(id);
        console.log(id);
      }


    } catch (e) {
      console.log(e);
      e.error.errors.messages.forEach(element => {

        let options = {
          title: "Oops!",
          subTitle: element,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      });
    }
  }
  async getNewById(id) {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    try {
      const response: any = await this.blogProvider.getNewById(id);
      console.log(response);
      response.likes = response.likes_cant;
      this.item = response;
      this.includeUserToSource(this.item.text);
      let obj = {
        newView: 1,
        index: this.index
      };
      this.callback(obj);
      loading.dismiss();
    } catch (e) {
      this.blogGetForParams.likes = this.blogGetForParams.likes_cant;
      this.item = this.blogGetForParams;
      console.log(this.item);
      loading.dismiss();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  HomeRedirect(val) {
    this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
  }

  showImageViewer(url: string, allData:any) {
    console.log(url);
    if (url) {
      let title: string = allData.title;
      let options = {
        share: true, // default is false
	      closeButton: true, // default is true
	      copyToReference: true, // default is false
        headers: "",  // If it is not provided, it will trigger an exception
        piccasoOptions: { } // If it is not provided, it will trigger an exception
      };
      this.photoViewer.show(url, title, options);
    }
  }
  async newLike(idBlog) {
    this.item.likes_cant++;
    let data = {
      it_news_id: idBlog,
      it_users_id: this.me.id
    };
    try {
      await this.blogProvider.newLike(data);
      this.item.my_like++;
      this.item.likes = this.item.likes + 1;
      let obj = {
        newLike: 1,
        index: this.index
      };
      this.callback(obj);
    } catch (e) {
      console.log(e.error.errors.messages[0]);
    }
  }
  showGallery(item){
    var images = []
    this.item.gallery_items[0].galleryimages.forEach(element => {
      if(element.image){
        const img = {
          url: this.AUTH.API_URL + "/" + element.image,
          title: this.item.gallery_items[0].name
        }
        images.push(img);
      }
    });
    var galleryOptions = {
      photos: images,
      closeIcon: "md-close-circle",
      initialSlide: 0
    };
    let modal = this.modalCtrl.create(GalleryModal, galleryOptions);

    this.platform.registerBackButtonAction(() => {
      if(modal.dismiss()){
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.pop();
        }, 2);
      }
    }, 2);
    modal.present();
  }
}
