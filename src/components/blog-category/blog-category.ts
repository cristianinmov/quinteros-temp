import { GeneralProvider } from "./../../providers/general/general";
import {
  NavController,
  NavParams,
  Platform,
  ViewController,
  AlertController,
  LoadingController
} from "ionic-angular";
import { BlogProvider } from "./../../providers/blog/blog";
import { StatusBar } from "@ionic-native/status-bar";
import { Component } from "@angular/core";
import { HomePage } from "../../pages/home/home";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "blog-category",
  templateUrl: "blog-category.html"
})
export class BlogCategoryComponent {
  text: string;
  itemsCategories: any;
  callback: any;
  data: any;
  category: any;
  MESSAGE_ERROR: any;
  wait: any;
  NOT_FOUND: boolean = false;

  constructor(
    public statusBar: StatusBar,
    public blogProvider: BlogProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public general: GeneralProvider
  ) {
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });

    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    this.getCategories();

    this.callback = this.navParams.get("callback");
    this.data = this.navParams.get("data") || [];
    this.category = this.navParams.get("category") || 0;
  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
      } else {
        this.navCtrl.pop();
      }
    }, 1);
  }

  goToHome() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: "forward" });
  }

  sendData(event: any, item: any): void {
    this.callback(item).then(() => {
      this.navCtrl.pop();
    });
  }

  async getCategories() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    try {
      const data: any = await this.blogProvider.getAllCategories();
      let array = [];
      Object.keys(data).map(key => {
        array.push(data[key]);
      });
      loading.dismiss();
      this.itemsCategories = array;
      if (this.itemsCategories.lenght == 0) {
        this.NOT_FOUND = true;
      }
    } catch (e) {
      this.NOT_FOUND = true;

      loading.dismiss();

      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            };
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              };
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          };
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
}
