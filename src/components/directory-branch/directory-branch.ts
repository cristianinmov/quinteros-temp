import { GeneralProvider } from "./../../providers/general/general";
import {
  NavController,
  NavParams,
  Platform,
  ViewController,
  AlertController,
  LoadingController
} from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { Component } from "@angular/core";
import { HomePage } from "../../pages/home/home";
import { TranslateService } from "@ngx-translate/core";
import { DirectoryProvider } from "../../providers/directory/directory";

/**
 * Generated class for the DirectoryBranchComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'directory-branch',
  templateUrl: 'directory-branch.html'
})
export class DirectoryBranchComponent {

  MESSAGE_ERROR: any;
  branches:any;
  NOT_FOUND:any = false;
  callback:any;
  branch:any;
  wait:any;

  constructor(
    public statusBar: StatusBar,
    public directoryProvider: DirectoryProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public general: GeneralProvider
  ){}

  ionViewDidLoad(){
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    this.callback = this.navParams.get("callback");
    this.branch = this.navParams.get("branch");
    this.getAllBranches();
  }

  async getAllBranches(){
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    try{
      const response : any = await this.directoryProvider.getAllBraches();
      if(response.data.length > 0){
        this.branches = response.data;
      }else{
        this.NOT_FOUND = false;
      }
      loading.dismiss();
    }catch(e){

      this.NOT_FOUND = true;

      loading.dismiss();

      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            };
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              };
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          };
          this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  async goToHome(){
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: "forward" });
  }

  sendData(event: any, item: any): void {
    this.callback(item).then(() => {
      this.navCtrl.pop();
    });
  }
}
