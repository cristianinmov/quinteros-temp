import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Storage } from "@ionic/storage";
import { StatusBar } from "@ionic-native/status-bar";
import {
  Platform,
  ActionSheetController,
  ToastController,
  NavController,
  AlertController,
  LoadingController,
  ViewController,
  Nav
} from "ionic-angular";
import { SavedComponent } from "./../digital-card-saved/saved";
import { Component, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { LoginPage } from "../../pages/login/login";

import { SocialSharing } from "@ionic-native/social-sharing";
import { Clipboard } from "@ionic-native/clipboard";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";

import { App } from "ionic-angular";

// Save contact
import {
  Contacts,
  Contact,
  ContactField,
  ContactName,
  ContactOrganization
} from "@ionic-native/contacts";
// Geolocalitation
import { Geolocation } from "@ionic-native/geolocation";
// Provaider Service Save Card
import { DigitalCardProvider } from "../../providers/digital-card/digital-card";

import { GeneralProvider } from "../../providers/general/general";
import { environment } from "../../environments/environment";

import { Sim } from "@ionic-native/sim";

// Select Search
import { IonicSelectableComponent } from "ionic-selectable";

// IntroJs
import introJs from "../../../node_modules/intro.js/intro";

class Indicative {
  public id: number;
  public name: string;
  public phonecode: number;
}

@Component({
  selector: "send",
  templateUrl: "send.html"
})
export class SendComponent {
  @ViewChild(Nav) nav: Nav;

  @ViewChild("port") portComponent: IonicSelectableComponent;
  ionicSelectableComponent: IonicSelectableComponent;
  API = environment.API;
  root = SavedComponent;
  text: string;
  showCompleteInfo: boolean = false;
  me: any;
  send: any = { phone: "", name: "", email: "", position: "", business: "" };
  dataEnd: any = [];
  link_card: any;
  generalTranslate: any;
  wait;
  latitud;
  longitud;
  id_user;
  id_company;
  position;
  company_name;
  prefix: any = [];
  flagFixSelected: any;
  prefixArray: any;
  indicative: Indicative;
  prefixPhone: any;
  public myGroup: FormGroup;
  id_placeholder: any;
  DATA: any;
  openModal: any = 0;
  prefixInitial:any;
  MESSAGE_ERROR:any;

  constructor(
    public statusBar: StatusBar,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public translate: TranslateService,
    public storage: Storage,
    public alertCtrl: AlertController,
    public navController: NavController,
    private socialSharing: SocialSharing,
    public contacts: Contacts,
    private clipboard: Clipboard,
    public loadingCtrl: LoadingController,
    public alerttCtrl: AlertController,
    public digitalCardProvider: DigitalCardProvider,
    private formBuilder: FormBuilder,
    private geolocation: Geolocation,
    public general: GeneralProvider,
    private sim: Sim,
    public app: App,
    public viewCtrl: ViewController,
    public androidPermissions: AndroidPermissions,
  ) {
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.prefixArray = [];

    this.getFlags();

    this.myGroup = this.formBuilder.group({
      mail: [
        "",
        Validators.compose([
          Validators.maxLength(70),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ]
    });
    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();
    this.storage
      .get("user_access")
      .then((data: any) => {
        this.link_card = data.card_url;
        this.me = data;
      })
      .catch((e: any) => {
        console.log(e);
      });

    // DataAll : Ontener arreglocon objetos
    this.storage.get("MDC_SAVED").then((dataAll: any) => {
      if (dataAll) {
        if (dataAll.length > 0) {
          dataAll.forEach(e => {
            this.dataEnd.push(e);
          });
        }
      } else {
        this.dataEnd = [];
      }
    });

    // var generalTranslate;
    this.translate.get("DATA").subscribe((res: string) => {
      this.generalTranslate = res;
    });
    this.wait = this.generalTranslate.WAIT;

    // console.log(this.portComponent)
    // this.portComponent.open();
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.translate.get("DATA").subscribe(value => {
        this.DATA = value;
      });
      this.storage.get("mdc_send_intro_finish").then((data: any) => {
        if (!data) {
          this.intro();
        }
      });
    });
  }

  myPrefix(arrayISO) {
    this.sim
      .getSimInfo()
      .then(info => {
        let prefixLocal;
        arrayISO.forEach(element => {
          if (element.iso == info.countryCode.toUpperCase()) {
            prefixLocal = element;
          }
        });

        this.prefixArray.unshift(prefixLocal);
        this.prefixInitial = this.prefixArray;
        this.indicative = this.prefixArray[0];
        this.prefixPhone = this.prefixArray[0].phonecode;
      })
      .catch(error => {
        let prefixLocal;
        arrayISO.forEach(element => {
          let contrycodeee = "co";
          if (element.iso === contrycodeee.toUpperCase()) {
            prefixLocal = element;
          }
        });
        // prefixLocal = {"id":228,"nombre":"Estados Unidos","name":"United States","code":"US","iso":"US","nicename":"United States","iso3":"USA","numcode":840,"phonecode":1,"status":"A","created_at":null,"updated_at":null,"deleted_at":null,"flag_small":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags\/us.png","flag_large":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-large\/us.png","flag_medium":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-medium\/us.png"};
        console.log("prefixLocal", prefixLocal)
        this.prefixArray.unshift(prefixLocal);
        this.indicative = this.prefixArray[0];
        this.prefixInitial = this.prefixArray;
        this.prefixPhone = this.prefixArray[0].phonecode;

        let toast = this.toastCtrl.create({
          message: error,
          duration: 2000,
          position: "bottom"
        });
        toast.present(toast);
      });
  }

  portChange(event: { component: IonicSelectableComponent; value: any }) {
    this.prefixPhone = event.value.phonecode;
    this.openModal = 0;
    this.platform.registerBackButtonAction(() => {
      this.navController.pop();
    }, 1);
  }

  async getFlags() {
    this.storage.get("flags").then((getRequest: any) => {
      this.prefixArray = [];
      this.myPrefix(getRequest);
    });
  }

  moreOptions() {
    var options;
    this.translate
      .get("DATA.DIGITAL_CARD.TABS.SEND")
      .subscribe((res: string) => {
        console.log(res);
        options = res;
      });
    let actionSheet = this.actionsheetCtrl.create({
      title: options.MORE_OPTIONS_SEND.SEND_VIA,
      cssClass: "action-sheets-basic-page",
      buttons: [
        {
          text: options.MORE_OPTIONS_SEND.SMS_TWILIO,
          icon: !this.platform.is("ios") ? "star" : null,
          handler: () => {
            if (this.send.phone) {
              let loading = this.loadingCtrl.create({
                content: this.wait
              });

              loading.present();
              this.storage.get("user_access").then(val => {
                // Share via twilio
                console.log("+" + this.prefixPhone + this.send.phone);
                let obj = {
                  phone: "+" + this.prefixPhone + this.send.phone,
                  message:
                    options.MESSAGE_MDC +
                    val.card_url +
                    ", atte. " +
                    val.name +
                    ""
                };

                try {
                  this.digitalCardProvider.sendSMSTwilio(obj);
                  // this.getAllcomentaries(this.commetnary);
                  // this.message_text = "";
                  //this.comments = "";
                  loading.dismiss();

                  let toast = this.toastCtrl.create({
                    message: this.generalTranslate.GENERAL_MESSAGES.OK
                      .TWILIO_SUCCESSFULLY,
                    duration: 2000,
                    position: "bottom"
                  });
                  toast.present(toast);

                  this.showCompleteInfo = true;

                  //this.CommunityPage.publications
                  // this.callback(this.index_of_community);
                } catch (e) {
                  // data.status = 1;
                  // this.contactSaveLocalStorage(data);

                  // loading.dismiss();
                  switch (e.status) {
                    case 401:
                      this.storage.remove("user_access");
                      this.storage.remove("METHOD_AUTH");
                      this.storage.remove("accessToken");
                      this.navController.setRoot(LoginPage);

                      break;

                    case 422:
                      let myText = "";
                      let array = Object.keys(e.error.errors).map(key => {
                        myText = myText + " " + e.error.errors[key][0];
                      });
                      for (let index = 0; index < array.length; index++) {
                        if (index + 1 == array.length) {
                          let options = {
                            title: "Oops!",
                            subTitle: myText,
                            buttons: ["OK"],
                            cssClass: "alertErrors"
                          };
                          this.general.presentAlertError(
                            options,
                            this.viewCtrl.name
                          );
                        }
                      }
                      break;
                    default:
                    //catch in te interceptor
                  }
                }

                // this.socialSharing.share(options.MESSAGE_MDC + val.card_url + ', atte. ' + val.name + '', 'My Digital Card').then(() => {
                //   // Success!
                // }).catch(() => {
                //   let toast = this.toastCtrl.create({
                //     message: this.generalTranslate.GENERAL_MESSAGES.ERROR_APPS,
                //     duration: 2000,
                //     position: 'bottom'
                //   });
                //   toast.present(toast);
                // });
              });
            } else {
              let options = {
                title: this.generalTranslate.DIGITAL_CARD.MESSAGES
                  .TITLE_EMPTY_FIELD,
                subTitle: this.generalTranslate.DIGITAL_CARD.MESSAGES
                  .CONTACT_EMPTY_FIELD,
                buttons: [
                  this.generalTranslate.DIGITAL_CARD.MESSAGES.CORRECT_BTN
                ],
                cssClass: "alertErrors"
              };
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
            console.log("My twilio SMS clicked");
          }
        },
        {
          text: options.MORE_OPTIONS_SEND.SMS,
          role: "destructive",
          icon: !this.platform.is("ios") ? "text" : null,
          handler: () => {
            this.storage.get("user_access").then(val => {
              if (this.send.phone) {
                console.log("+" + this.prefixPhone + this.send.phone);

                this.socialSharing
                  .shareViaSMS(
                    options.MESSAGE_MDC +
                      val.card_url +
                      ", atte. " +
                      val.name +
                      "",
                    "+" + this.prefixPhone + this.send.phone
                  )
                  .then(() => {
                    // Success!
                  })
                  .catch(() => {
                    let toast = this.toastCtrl.create({
                      message: this.generalTranslate.GENERAL_MESSAGES.ERROR_SMS,
                      duration: 2000,
                      position: "bottom"
                    });
                    toast.present(toast);
                  });
                console.log(val.card_url);
              } else {
                let options = {
                  title: this.generalTranslate.DIGITAL_CARD.MESSAGES
                    .TITLE_EMPTY_FIELD,
                  subTitle: this.generalTranslate.DIGITAL_CARD.MESSAGES
                    .CONTACT_EMPTY_FIELD,
                  buttons: [
                    this.generalTranslate.DIGITAL_CARD.MESSAGES.CORRECT_BTN
                  ],
                  cssClass: "alertErrors"
                };
                this.general.presentAlertError(options, this.viewCtrl.name);
              }
            });
            console.log("SMS clicked");
          }
        },
        {
          text: options.MORE_OPTIONS_SEND.WHATSAPP,
          icon: !this.platform.is("ios") ? "logo-whatsapp" : null,
          handler: () => {
            // if()
            this.storage.get("user_access").then(val => {
              // Share via email
              if (this.send.phone) {
                this.socialSharing
                  .shareViaWhatsAppToReceiver(
                    "+" + this.prefixPhone + this.send.phone,
                    options.MESSAGE_MDC +
                      val.card_url +
                      ", atte. " +
                      val.name +
                      ""
                  )
                  .then(() => {
                    // Success!
                  })
                  .catch(() => {
                    let toast = this.toastCtrl.create({
                      message: this.generalTranslate.GENERAL_MESSAGES
                        .ERROR_WHATSAPP,
                      duration: 2000,
                      position: "bottom"
                    });
                    toast.present(toast);
                  });
              } else {
                let options = {
                  title: this.generalTranslate.DIGITAL_CARD.MESSAGES
                    .TITLE_EMPTY_FIELD,
                  subTitle: this.generalTranslate.DIGITAL_CARD.MESSAGES
                    .CONTACT_EMPTY_FIELD,
                  buttons: [
                    this.generalTranslate.DIGITAL_CARD.MESSAGES.CORRECT_BTN
                  ],
                  cssClass: "alertErrors"
                };
                this.general.presentAlertError(options, this.viewCtrl.name);
              }

              // window.location="https://wa.me/57"+val.phone+"?text=Hola%20te%20comparto%20mi%20tarjeta%20de%20contacto%20"+val.card_url+", atte. "+val.name+"";

              // console.log(val.card_url)
              // $("a").attr("href", "intent://send/+905055555#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end");
            });
            console.log("WhatsApp clicked");
          }
        },
        {
          text: options.MORE_OPTIONS_SEND.MY_APPS,
          icon: !this.platform.is("ios") ? "apps" : null,
          handler: () => {
            this.storage.get("user_access").then(val => {
              // Share via email
              this.socialSharing
                .share(
                  options.MESSAGE_MDC +
                    val.card_url +
                    ", atte. " +
                    val.name +
                    "",
                  "Los Quintero App"
                )
                .then(() => {
                  // Success!
                })
                .catch(() => {
                  let toast = this.toastCtrl.create({
                    message: this.generalTranslate.GENERAL_MESSAGES.ERROR_APPS,
                    duration: 2000,
                    position: "bottom"
                  });
                  toast.present(toast);
                });
            });

            console.log("My apps clicked");
          }
        },
        {
          text: options.MORE_OPTIONS_SEND.CANCEL,
          role: "cancel", // will always sort to be on the bottom
          icon: !this.platform.is("ios") ? "close" : null,
          cssClass: "color-cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
  showToast(position: string) {
    this.storage.get("user_access").then(val => {
      this.clipboard.copy(val.card_url);
    });
    var copied;
    this.translate
      .get("DATA.DIGITAL_CARD.TABS.SEND")
      .subscribe((res: string) => {
        console.log(res);
        copied = res;
      });

    let toast = this.toastCtrl.create({
      message: copied.MESSAGES.COPIED,
      duration: 2000,
      position: position
    });
    toast.present(toast);
  }
  completeInfo() {
    if (this.showCompleteInfo) {
      this.showCompleteInfo = false;
    } else {
      this.showCompleteInfo = true;
    }
  }

  async saveContact(data) {

    await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION &&
      this.androidPermissions.PERMISSION.READ_CONTACTS &&
      this.androidPermissions.PERMISSION.WRITE_CONTACTS &&
      this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION &&
      this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE &&
      this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      ).then(
      (result:any) => {
        console.log('Has general permission?', result.hasPermission)
        if(result.hasPermission == false){
          this.androidPermissions.requestPermissions([
            this.androidPermissions.PERMISSION.READ_CONTACTS,
            this.androidPermissions.PERMISSION.WRITE_CONTACTS,
            this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
            this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
            this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          ]);
          return;
        }
      }
    );

    console.log(this.flagFixSelected);

    this.contactCloudService(data); //post
    this.contactSaveLocalStorage(data);
    this.contactCreate(data); //1
  }

  formatDate() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = date.getMonth() + 1;

    if (gg < 10) gg = 0 + gg;

    if (mm < 10) mm = 0 + mm;

    var cur_day = aaaa + "-" + mm + "-" + gg;

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    if (hours < 10) hours = 0 + hours;

    if (minutes < 10) minutes = 0 + minutes;

    if (seconds < 10) seconds = 0 + seconds;

    let fechaFinal = cur_day + " " + hours + ":" + minutes + ":" + seconds;

    return fechaFinal;
  }

  contactCreate(data) {
    if (this.send.phone != "") {
      if (this.send.name != "") {
        let contact: Contact = this.contacts.create();

        contact.name = new ContactName(null, "", data.name);
        contact.phoneNumbers = [new ContactField("mobile", data.phone)];
        // contact.name = new ContactName(null, '', data.name);
        contact.organizations = [
          new ContactOrganization("name", data.business),
          new ContactOrganization("title", data.position)
        ];
        contact.emails = [new ContactField("email", data.email)];
        // contact.emails = [new ContactField('email', data.email)];
        contact.note = this.generalTranslate.DIGITAL_CARD.TABS.SEND.MESSAGE_NOTE_CONTACT;

        contact
          .save()
          .then(
            () => console.log("Contact saved!", contact),
            (error: any) => console.error("Error saving contact.", error)
          );
      }
    }
  }

  async contactSaveLocalStorage(data) {
    if (this.send.phone != "") {
      if (this.send.name != "") {
        this.dataEnd = [];
        await this.storage.get("MDC_SAVED").then((dataAll: any) => {
          if (dataAll) {
            if (dataAll.length > 0) {
              dataAll.forEach((e, i) => {
                if (e.phone == data.phone) {
                  // Update contact
                  console.log(data);
                  data.data;
                } else {
                  // new contact
                  this.dataEnd.push(e);
                }
              });

              console.log(dataAll);
              console.log(this.dataEnd);
            }
          } else {
            this.dataEnd = [];
          }
        });
        data.status = 1;
        data.registre_time = this.formatDate();

        await this.geolocation
          .getCurrentPosition()
          .then(resp => {
            this.latitud = resp.coords.latitude;
            this.longitud = resp.coords.longitude;
          })
          .catch(error => {
            console.log("Error getting location", error);
          });

        await this.storage.get("user_access").then(value => {
          data.it_users_id = value.id;
          this.id_company = value.company[0].id;
          // this.company_name = value.company[0].name;
          // this.position = value.position[0].name;
        });
        data.it_business_id = this.id_company;
        data.country_phone = "+" + this.prefixPhone;
        data.latitude = this.latitud;
        data.longitude = this.longitud;
        await this.dataEnd.push(data);
        this.dataEnd = await this.array_move(this.dataEnd, 19, 0);
        this.storage.set("MDC_SAVED", this.dataEnd);
        data = {};

        // toast response
        let toast = this.toastCtrl.create({
          message: this.generalTranslate.GENERAL_MESSAGES.OK
            .CONTACT_SAVED_SUCCESSFULLY,
          duration: 2000,
          position: "bottom"
        });
        toast.present(toast);

        this.send = {
          phone: "",
          name: "",
          email: "",
          position: "",
          business: ""
        };
        this.showCompleteInfo = false;
      }
    }
  }

  async contactCloudService(data) {
    console.log("model:", this.indicative);
    console.log("model2:", this.prefixPhone);
    if (data.phone != "") {
      if (data.name != "") {
        //MessageToast
        let loading = this.loadingCtrl.create({
          content: this.wait
        });
        loading.present();

        // Geolocalitation
        await this.geolocation
          .getCurrentPosition()
          .then(resp => {
            this.latitud = resp.coords.latitude;
            this.longitud = resp.coords.longitude;
          })
          .catch(error => {
            console.log("Error getting location", error);
          });

        //  Other data [id_user, id_company]
        await this.storage.get("user_access").then((dataAll: any) => {
          let tempVar = [{ name: "" }];
          if (dataAll.position.length <= 0) {
            dataAll.position = tempVar;
          }
          if (dataAll.company.length <= 0) {
            dataAll.company = tempVar;
          }
          this.id_user = dataAll.id;
          this.id_company = dataAll.company[0].id;
          this.company_name = dataAll.company[0].name;
          this.position = dataAll.position[0].name;
        });

        let fecha = this.formatDate();

        console.log(data);

        let obj = {
          it_business_id: this.id_company,
          phone: data.phone,
          country_phone: "+" + this.prefixPhone,
          name: data.name,
          position: data.position,
          business: data.business,
          email: data.email,
          registre_time: fecha,
          latitude: this.latitud,
          longitude: this.longitud,
          it_users_id: this.id_user
        };
        try {
          await this.digitalCardProvider.saveMDC(obj);
          loading.dismiss();
        } catch (e) {
          loading.dismiss();
        }
      } else {
        let options = {
          title: this.generalTranslate.DIGITAL_CARD.MESSAGES.TITLE_EMPTY_FIELD,
          subTitle: this.generalTranslate.DIGITAL_CARD.MESSAGES
            .FULLNAME_EMPTY_FIELD,
          buttons: [
            {
              text: this.generalTranslate.DIGITAL_CARD.MESSAGES.CORRECT_BTN,
              cssClass: "alerCtrlBtnPrimary"
            }
          ]
        };
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    } else {
      let alert = this.alertCtrl.create({
        title: this.generalTranslate.DIGITAL_CARD.MESSAGES.TITLE_EMPTY_FIELD,
        subTitle: this.generalTranslate.DIGITAL_CARD.MESSAGES
          .CONTACT_EMPTY_FIELD,
        buttons: [
          {
            text: this.generalTranslate.DIGITAL_CARD.MESSAGES.CORRECT_BTN,
            cssClass: "alerCtrlBtnPrimary"
          }
        ]
      });
      alert.present();
    }
  }

  intro() {
    let me = this;
    let intro = introJs();
    intro.setOptions({
      steps: [
        {
          element: "#select_prefix_phone",
          intro:
            "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>" +
            this.DATA.TOUR.MDC.SEND.PREFIX_PHONE +
            "</span>",
          position: "bottom"
        },
        {
          element: "#send_digital_card",
          intro:
            "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>" +
            this.DATA.TOUR.MDC.SEND.SEND_DIGITAL_CARD +
            "</span>",
          position: "bottom"
        },
        {
          element: "#copy_my_digital_card",
          intro:
            "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>" +
            this.DATA.TOUR.MDC.SEND.COPY_DIGITAL_CARD +
            "</span>",
          position: "bottom"
        }
      ],
      showBullets: false,
      showStepNumbers: false,
      hidePrev: true,
      hideNext: true,
      nextLabel:
        "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>" +
        this.DATA.TOUR.NEXT +
        "</span>",
      prevLabel:
        "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>" +
        this.DATA.TOUR.PREVIOUS +
        "</span>",
      skipLabel:
        "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>" +
        this.DATA.TOUR.SKIP +
        "</span>",
      doneLabel:
        "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>" +
        this.DATA.TOUR.DONE +
        "</span>"
    });
    intro.oncomplete(function() {
      me.storage.set("mdc_send_intro_finish", true);
    });
    intro.onexit(function() {
      me.storage.set("mdc_send_intro_finish", true);
    });
    // intro.start();
  }
  openModalFlags() {
    let me = this;
    this.platform.registerBackButtonAction(() => {
      if (me.portComponent.close()) {
        console.log("close modal");
        this.platform.registerBackButtonAction(() => {
          this.app.navPop();
        }, 1);
      }
      me.openModal == 0;
    }, 1);
  }

  onClose(event: { component: IonicSelectableComponent; value: any }) {
    console.log(event.component.value);
    this.platform.registerBackButtonAction(() => {
      this.app.navPop();
    }, 2);
  }
  array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
      var k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
  }
  async searchPorts(event:{
    component: IonicSelectableComponent,
    text: string
  }){
    event.component.startSearch();
    if(event.text.length >= 3){
      try{
        const flags:any = await this.digitalCardProvider.getFlagsByName(event.text)
        if(flags.data.length > 0){
          this.prefixArray = flags.data;
        }else{
          this.prefixArray = [];
        }
      }
      catch(e){

        switch (e.status) {
          case 401:
            this.storage.remove("user_access");
            this.storage.remove("METHOD_AUTH");
            this.storage.remove("accessToken");
            this.nav.setRoot(LoginPage, {}, { animate: true, direction: "forward" });
            e.error.errors.messages.forEach(element => {
              let options = {
                title: "Oops!",
                subTitle: element,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            });

            break;

          case 422:
            let myText = "";
            let array = Object.keys(e.error.errors).map(key => {
              myText = myText + " " + e.error.errors[key][0];
            });
            for (let index = 0; index < array.length; index++) {
              if (index + 1 == array.length) {
                let options = {
                  title: "Oops!",
                  subTitle: myText,
                  buttons: ["OK"],
                  cssClass: "alertErrors"
                }
                this.general.presentAlertError(options, this.viewCtrl.name);
              }
            }
            break;
          default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
          //catch in te interceptor
        }
      }
    }else{
      this.prefixArray = [this.indicative];
    }
    event.component.endSearch();
  }
}
