import { GeneralProvider } from './../../providers/general/general';
import { CommentaryComponent } from './../commentary/commentary';
import { environment } from "./../../environments/environment";
import { Component } from "@angular/core";
import { NavController, NavParams, LoadingController, AlertController } from "ionic-angular";
import { HomePage } from "../../pages/home/home";
import { CommutyProvider } from "../../providers/commuty/commuty";
import { StatusBar } from "@ionic-native/status-bar";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { LoginPage } from '../../pages/login/login';
import { ViewController } from 'ionic-angular';

@Component({
  selector: "community-detail",
  templateUrl: "community-detail.html"
})
export class CommunityDetailComponent {
  text: string;
  item: any = {
    photo: '',
    user: {
      avatar: '',
      name: '',
      last_name: '',
    },
    date: '',
    text: '',
    likes_cant: '',
    comments_cant: '',
    my_like: ''
  }
  me: any;
  MESSAGE_ERROR: any;
  wait: any;

  API = environment.API;
  USER_DEFAULT = environment.USER_DEFAULT;
  PUBLICATION_DEFAULT = environment.PUBLICATION_DEFAULT;
  infoToNotification:any;
  comments_cant:any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public commutyProvider: CommutyProvider,
    public statusBar: StatusBar,
    public storage: Storage,
    public translate: TranslateService,
    public photoViewer: PhotoViewer,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public general: GeneralProvider,
    public viewCtrl: ViewController
  ) {

    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    this.storage
      .get("user_access")
      .then((data: any) => {
        this.me = data;
      })
      .catch((e: any) => {
        console.log(e);
      });
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });


  }

  ionViewDidLoad(){
    if(this.navParams.get("obj")){
      this.item = this.navParams.get("obj");
      console.log(this.item);
    }

    this.getNotofications();
  }

  async getNotofications(){
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    this.infoToNotification = await this.navParams.get("infoToNotification");
    console.log(this.infoToNotification);
    if(!this.infoToNotification){
      loading.dismiss();
      return;
    }
    try{
      const response = await this.commutyProvider.getCommunityById(this.infoToNotification.additionalData.item_id)
      this.item = response;
      loading.dismiss();
    }catch (e){
      loading.dismiss();

      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
        //catch in te interceptor
      }
    }

  }

  HomeRedirect() {
    this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
  }

  async newLike(item) {
    console.log(item);
    let data = {
      it_publication_id: item.id,
      it_users_id: this.me.id
    };

    try {
      const response: any = await this.commutyProvider.newLike(data);
      item.likes_cant++;
      item.my_like = 1;
      console.log(response);
    } catch (e) {
      console.log(e.error.errors.messages[0]);
    }
  }

  comments(data) {
    this.navCtrl.push(CommentaryComponent, {
      community: data,
      callback: this.getData
    });
  }
  getData = data => {
    return new Promise((resolve, reject) => {
      console.log(data);
      this.comments_cant++;
      resolve();
    });
  }

  showImageViewer(url: string, allData: any) {
    if (url) {
      let title: string =
        allData.user.name + " " + allData.user.last_name + " - " + allData.text;
      let options = {
        share: true, // default is false
        closeButton: true, // default is true
        copyToReference: true, // default is false
        headers: "",  // If it is not provided, it will trigger an exception
        piccasoOptions: { } // If it is not provided, it will trigger an exception
      };
      this.photoViewer.show(url, title, options);
    }
  }
}
