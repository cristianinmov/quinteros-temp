import { TranslateService } from "@ngx-translate/core";
import { Storage } from "@ionic/storage";
import { AuthProvider } from "./../providers/auth/auth";
import { Globalization } from "@ionic-native/globalization";
import { LoginPage } from "./../pages/login/login";
import { ProfilePage } from "./../pages/profile/profile";
import { Component, ViewChild } from "@angular/core";
import {
  Nav,
  Platform,
  ToastController,
  AlertController,
  ModalController
} from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { environment } from "../environments/environment";
import { Network } from "@ionic-native/network";
import { HomePage } from "../pages/home/home";
import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { OcrPage } from "../pages/ocr/ocr";
import { WelcomePage } from "../pages/welcome/welcome";
import { OneSignal } from "@ionic-native/onesignal";

declare var Appsee:any;

console.log('INGRESE 2')
@Component({
  templateUrl: "app.html"
})
export class MyApp {
  API = environment.API;
  USER_DEFAULT = environment.USER_DEFAULT;
  me: any = {
    avatar: null
  };
  STATUS_ONLINE: any;
  STATUS_OFFLINE: any;
  @ViewChild(Nav) nav: Nav;
  DATA: any;

  rootPage: any = WelcomePage;

  pages: Array<{ title: string; component: any }>;
  AUTH = environment.API;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public toastCtrl: ToastController,
    public globalization: Globalization,
    public authProvider: AuthProvider,
    public storage: Storage,
    public translate: TranslateService,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public network: Network,
    public oneSignal: OneSignal,
    public screenOrientation: ScreenOrientation,
  ) {
    this.platform.ready().then(async () => {
      this.getLamg();
      this.splashScreen.hide()
      this.statusBar.styleDefault();
      Appsee.start('b5e8fa53e4de4d738fd821e9bc2d834d');
      this.oneSignal.startInit(
        "b8395d0b-9776-4ccb-b26b-5773cdc5850f",
        "quintero-38ab4"
      );
    });

    //Language
    this.translate.setDefaultLang("es");
    this.getMesssageNetwork();
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [{ title: "My Profile", component: ProfilePage }];

    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
  }

  ionViewDidLoad() {
    this.splashScreen.show();
  }
  async valideView() {
    this.storage.get("accessToken").then((data: any) => {
      this.API.AUTH = data;
      this.authProvider
        .getMe()
        .then(() => {
          this.rootPage = HomePage;
        })
        .catch((data: any) => {
          console.log(data);
          this.rootPage = LoginPage;
        });
    });
  }

  async getMesssageNetwork() {
    await this.translate.get("DATA.NETWORK.STATUS_ONLINE").subscribe(value => {
      this.STATUS_ONLINE = value;
    });
    await this.translate.get("DATA.NETWORK.STATUS_OFFLINE").subscribe(value => {
      this.STATUS_OFFLINE = value;
    });

    //NETWORK
    this.network.onDisconnect().subscribe(() => {
      //Desconectado de internet
    });
    this.network.onConnect().subscribe(() => {
      //Desconectado de internet
    });
  }
  getImage() {
    this.storage
      .get("user_access")
      .then((data: any) => {
        this.me = data;
      })
      .catch((e: any) => {
        console.log(e);
      });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is("android")) {
        this.screenOrientation.lock("portrait");
      }

      this.nav.resize();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
  goToProfile() {
    this.nav.push(ProfilePage);
  }
  goToOCR(){
    this.nav.push(OcrPage);
  }
  goToLogin() {
    this.nav.push(LoginPage);
  }

  async logout() {
    try {
      const type: any = await this.storage.get("METHOD_AUTH");

      switch (type.value) {
        case "0":
          try {
            const data: any = await this.authProvider.signOutService();
            this.storage.remove("user_access");
            this.storage.remove("METHOD_AUTH");
            this.storage.remove("accessToken");

            let toast = this.toastCtrl.create({
              message: data.message,
              position: "button",
              duration: 3000
            });

            toast.present(toast);
          } catch (e) {
            console.log(e);
            this.logOutClearData();
            e.error.errors.messages.forEach(element => {
              const alert = this.alertCtrl.create({
                title: "Oops!",
                subTitle: element,
                buttons: ["OK"],
                cssClass: "alertErrors"
              });

              alert.present();
            });
          }
          break;
        case "1":
          try {
          } catch (e) {
            console.log(e);
          }
          break;
      }
    } catch (e) {
      console.log(e);
    }

    this.logOutClearData();
  }


  logOutClearData() {
    this.storage.remove("user_access");
    this.storage.remove("METHOD_AUTH");
    this.storage.remove("accessToken");
    this.nav.setRoot(LoginPage);
  }
  updateUrl(me) {
    me.avatar = null;
  }
  async closeApp() {
    /* const data: any = await this.globalization.getPreferredLanguage();
    const lang: string = data.value.charAt(0) + data.value.charAt(1); */
    const lang = 'es';
    await this.translate.use(lang);

    await this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });

    const prompt = this.alertCtrl.create({
      title: this.DATA.ACTION_BACK.TITLE,
      message: this.DATA.ACTION_BACK.DESCRIPTION,
      buttons: [
        {
          text: this.DATA.ACTION_BACK.BTN_CANCEL,
          role: "cancel",
          handler: () => {
            console.log("Application exit prevented!");
          },
          cssClass: "alerCtrlBtnPrimary"
        },
        {
          text: this.DATA.ACTION_BACK.BTN_EXIT,
          handler: () => {
            this.platform.exitApp(); // Close this application
          },
          cssClass: "alerCtrlBtnPrimary"
        }
      ]
    });
    prompt.present();
  }
  async getLamg(){
    const data: any = await this.globalization.getLocaleName();
    //const lang: string = data.value.charAt(0) + data.value.charAt(1);
    const lang: string = 'es';
    this.storage.set("lang", lang);
    this.translate.use(lang);
    this.AUTH.LANG = lang;
  }
}
