import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchContacts'
})

export class SearchPipe implements PipeTransform {
  public transform(value, args: string) {
    if(!value) {
      return;
    }
    if (!args) {
      return value;
    }
    args = args.toLowerCase();

    let result  =  value.filter( (item) => {
      return JSON.stringify(item).toLowerCase().includes(args);
    }, (err) => {
      console.log(err);
    })
    if(result.length === 0) {
      return [-1];
    }
    return result;
  }
}
