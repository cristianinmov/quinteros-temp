import { WelcomePageModule } from './../pages/welcome/welcome.module';
import { DirectoryContactComponent } from "./../components/directory-contact/directory-contact";
import { BlogDetailComponent } from "./../components/blog-detail/blog-detail";
import { BlogCategoryComponent } from "./../components/blog-category/blog-category";
import { CommunityNewPublicationComponent } from "./../components/community-new-publication/community-new-publication";
import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
  HttpClient
} from "@angular/common/http";
import { Globalization } from "@ionic-native/globalization";
import { SendComponent } from "./../components/digital-card-send/send";
import { DirectoryPageModule } from "./../pages/directory/directory.module";
import { BlogPageModule } from "./../pages/blog/blog.module";
import { CommunityPageModule } from "./../pages/community/community.module";
import { DigitalCardPageModule } from "./../pages/digital-card/digital-card.module";
import { LoginPageModule } from "./../pages/login/login.module";
import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { ProfilePageModule } from "../pages/profile/profile.module";
import { FileService } from '../pages/profile/file.service';
import { FormsModule } from "@angular/forms";
import { SavedComponent } from "../components/digital-card-saved/saved";
import { RegisterComponent } from "../components/register/register";
import { AuthProvider } from "../providers/auth/auth";
import { InterceptorProvider } from "../providers/interceptor/interceptor";
import { IonicStorageModule } from "@ionic/storage";

import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
import { AppVersion } from "@ionic-native/app-version";


/*
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireDatabaseModule } from "angularfire2/database";
*/

import { Camera } from "@ionic-native/camera";

/*
 * FireBase
 */

import { UserProvider } from "../providers/user/user";
import { BlogProvider } from "../providers/blog/blog";
import { CommutyProvider } from "../providers/commuty/commuty";
import { DigitalCardProvider } from "../providers/digital-card/digital-card";
import { DirectoryProvider } from "../providers/directory/directory";
import { UniqueDeviceID } from "@ionic-native/unique-device-id";

import { Device } from "@ionic-native/device";

import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { FilePath } from "@ionic-native/file-path";

import { TimeAgoPipe } from "time-ago-pipe";

import { Network } from "@ionic-native/network";
import { IonicImageLoader } from "ionic-image-loader";


//Photo Viewer
import { PhotoViewer } from '@ionic-native/photo-viewer';

//screen rotate
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { CommentaryComponent } from '../components/commentary/commentary';

// import { SMS } from '@ionic-native/sms';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

//Component ForgotPassword
import { ForgotPaswordComponent } from '../components/forgot-pasword/forgot-pasword';

// Component International Input
//import { Ng2TelInputModule } from 'ng2-tel-input';
import { GeneralProvider } from '../providers/general/general';

// Permissions
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { AppPermissionComponent } from '../components/app-permission/app-permission';

// Contacts
import { Contacts } from '@ionic-native/contacts';

// Geolocalitation
import { Geolocation } from '@ionic-native/geolocation';

//OneSignal
import { OneSignal } from '@ionic-native/onesignal';

//Keyboard
import { Keyboard } from '@ionic-native/keyboard';
import { CommunityDetailComponent } from "../components/community-detail/community-detail";

//Sim Lecture
import { Sim } from '@ionic-native/sim';
// Select Search
import { IonicSelectableModule } from 'ionic-selectable';

// ProgrssBar
import { NgProgressModule } from '@ngx-progressbar/core';
import { OcrPageModule } from "../pages/ocr/ocr.module";

//Base64
import { Base64 } from '@ionic-native/base64';

//Gallery Foto
import * as ionicGalleryModal from 'ionic-gallery-modal';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { DirectoryBranchComponent } from "../components/directory-branch/directory-branch";
import { TermsAndConditionsComponent } from '../components/terms-and-conditions/terms-and-conditions';
import { BlokedUsersComponent } from '../components/bloked-users/bloked-users';

//Market
import { Market } from '@ionic-native/market';
import { ComponentsModule } from '../components/components.module';
import { CalendarPageModule } from '../pages/calendar/calendar.module';
import { GenealogiaPageModule } from '../pages/genealogia/genealogia.module';
import { PagesCalendarApiCalendarProvider } from '../providers/pages-calendar-api-calendar/pages-calendar-api-calendar';
import { GenealogiaProvider } from '../providers/genealogia/genealogia';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    IonicSelectableModule,
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      statusbarPadding: true,
      backButtonText: 'Atras',
    }),
    NgProgressModule.forRoot(),
    LoginPageModule,
    ProfilePageModule,
    DigitalCardPageModule,
    CommunityPageModule,
    CalendarPageModule,
    GenealogiaPageModule,
    BlogPageModule,
    DirectoryPageModule,
    FormsModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),

    /*
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    */
    IonicImageLoader.forRoot(),
    OcrPageModule,
    ionicGalleryModal.GalleryModalModule,
    WelcomePageModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SendComponent,
    SavedComponent,
    RegisterComponent,
    CommunityNewPublicationComponent,
    BlogCategoryComponent,
    BlogDetailComponent,
    DirectoryContactComponent,
    CommentaryComponent,
    ForgotPaswordComponent,
    AppPermissionComponent,
    CommunityDetailComponent,
    DirectoryBranchComponent,
    TermsAndConditionsComponent,
    BlokedUsersComponent
  ],
  providers: [
    FileService,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Globalization,
    StatusBar,
    AuthProvider,
    InterceptorProvider,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorProvider, multi: true },
    AppVersion,
    FileTransfer,
    FileTransferObject,
    File,
    UserProvider,
    BlogProvider,
    CommutyProvider,
    DigitalCardProvider,
    DirectoryProvider,
    UniqueDeviceID,
    Device,
    MyApp,
    Camera,
    FilePath,
    TimeAgoPipe,
    Network,
    PhotoViewer,
    ScreenOrientation,
    SocialSharing,
    Clipboard,
    //Ng2TelInputModule,
    GeneralProvider,
    AndroidPermissions,
    Contacts,
    Geolocation,
    OneSignal,
    Keyboard,
    Sim,
    Base64,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ionicGalleryModal.GalleryModalHammerConfig,
    },
    Market,
    PagesCalendarApiCalendarProvider,
    GenealogiaProvider
  ]
})
export class AppModule { }
