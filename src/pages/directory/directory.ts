import { LoginPage } from "./../login/login";
import { environment } from "./../../environments/environment";
import { DirectoryContactComponent } from "./../../components/directory-contact/directory-contact";
import { DirectoryProvider } from "./../../providers/directory/directory";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Platform,
  ViewController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { StatusBar } from "@ionic-native/status-bar";
import { GeneralProvider } from "../../providers/general/general";
import { DirectoryBranchComponent } from "../../components/directory-branch/directory-branch";

/**
 * Generated class for the DirectoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-directory",
  templateUrl: "directory.html"
})
export class DirectoryPage {
  @ViewChild("myInput") myInput;
  AUTH = environment.API;
  name: string = "";
  isOn: any = false;
  items: any;
  USER_DEFAULT = environment.USER_DEFAULT;
  wait;
  groupedContacts = [];
  MESSAGE_ERROR:any;
  data: any;
  userAtributes: any[] = [];
  branch:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public directoryProvider: DirectoryProvider,
    public storage: Storage,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public statusBar: StatusBar,
    public platform: Platform,
    public viewCtrl: ViewController,
    public general: GeneralProvider
  ) {
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });

    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();
  }

  ionViewDidEnter() {
    // this.general.refreshToken();
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
      } else {
        this.navCtrl.pop();
      }
    }, 1);
  }

  async getLangMessage() {
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
      this.getToken();
    });
  }

  async getToken() {
    const data: any = await this.storage.get("accessToken");
    this.AUTH.AUTH = data;
    this.storage.get("branchesSelected").then((data:any) =>{
      if(data){
        this.branch = data.id;
        this.data = data.name;
        this.getUsersByBranch(data)
      }else{
        this.getAllUserDirectoy();
      }
    })
  }
  async getAllUserDirectoy() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();
    try {
      const data: any = await this.directoryProvider.getAllUSers();
      await this.groupContacts(data.data);
      this.items = await data.data;
      loading.dismiss();
    } catch (e) {
      loading.dismiss();
      this.storage.get("directory").then((data:any)=>{
        this.items = data;
        this.groupedContacts = data;
      })
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);

            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  async doRefresh(refresher) {
    try {
      var data:any;
      if(this.branch){
        this.getUsersByBranch({id: this.branch});
        refresher.complete();
        return;
      }else{
        data = await this.directoryProvider.getAllUSers();
      }
      this.groupedContacts = [];
      await this.groupContacts(data.data);
      this.items = data.data;
      refresher.complete();
    } catch (e) {
      refresher.complete();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  ionViewDidLoad() {
    this.getLangMessage();
  }
  itemSelected(item: object) {}
  searchBar() {
    if (this.isOn) {
      this.isOn = false;
    } else {
      this.isOn = true;
      setTimeout(() => {
        this.myInput.setFocus();
      }, 150);
    }
  }
  directoyContact(contact) {
    this.navCtrl.push(DirectoryContactComponent, {
      user: contact.id,
      image_url: contact.avatar,
      contact: contact
    });
  }
  updateUrl(item) {
    item.avatar = null;
  }
  groupContacts(contacts) {
    this.groupedContacts = [];
    let sortedContacts = contacts.sort();
    let currentLetter = false;
    let currentContacts = [];

    sortedContacts.forEach((value, index) => {
      delete value.birthday;
      delete value.country_phone;
      delete value.date_password_change;
      delete value.first_login;
      delete value.it_branches_id;
      delete value.it_positions_id;
      delete value.it_profile_id;
      delete value.languaje;
      delete value.last_login;
      delete value.login_alt;
      delete value.model;
      delete value.platform;
      if (value.name.charAt(0) != currentLetter) {
        currentLetter = value.name.charAt(0);

        let newGroup = {
          letter: currentLetter,
          contacts: []
        };

        currentContacts = newGroup.contacts;
        this.groupedContacts.push(newGroup);
      }

      currentContacts.push(value);
    });
    this.groupedContacts.sort();
    this.storage.set("directory", this.groupedContacts)
  }
  filterByBranch(){
    this.navCtrl.push(DirectoryBranchComponent, {
      callback: this.getData,
      branch: this.branch,
      data: this.data
    });
  }
  //CallBack fot get info of DirectoryBranchComponent
  getData = data => {
    return new Promise((resolve, reject) => {
      this.getUsersByBranch(data);
      this.storage.set("branchesSelected", data)
      this.branch = data;
      if(data){
        this.branch = data.id;
        this.data = data.name;
      }
      resolve();
    });
  };

  async getUsersByBranch(branch){
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();

    try{
      const users : any = await this.directoryProvider.getUsersByBranch(branch)
      await this.groupContacts(users.data);
      this.items = await users.data;
      loading.dismiss();
    }
    catch(e){
      loading.dismiss();
      this.storage.get("directory").then((data:any)=>{
        this.items = data;
        this.groupedContacts = data;
      })
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);

            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  removeFilter() {
    this.storage.set("branchesSelected", 0);
    this.data = "";
    this.branch = 0;
    this.getAllUserDirectoy();
  }
}
