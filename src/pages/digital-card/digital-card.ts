import { Storage } from "@ionic/storage";
import { StatusBar } from "@ionic-native/status-bar";
import { DigitalCardProvider } from "./../../providers/digital-card/digital-card";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ViewController,
  AlertController
} from "ionic-angular";
import { SendComponent } from "../../components/digital-card-send/send";
import { SavedComponent } from "../../components/digital-card-saved/saved";
import { GeneralProvider } from "../../providers/general/general";
import { environment } from "../../environments/environment";

// import { GeneralProvider } from '../../providers/general/general';

@IonicPage()
@Component({
  selector: "page-digital-card",
  templateUrl: "digital-card.html"
})
export class DigitalCardPage {
  API = environment.API;

  tab1Root: any = "";
  tab2Root: any = "";
  contacts: any;
  dataEnd: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public digitalCardProvider: DigitalCardProvider,
    public statusBar: StatusBar,
    public general: GeneralProvider,
    public platform: Platform,
    public storage: Storage,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController
  ) {
    this.tab1Root = SendComponent;
    this.tab2Root = SavedComponent;
    this.digitalCardProvider;

    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    this.verificationLocalData();
  }

  ionViewDidLoad() {}
  ionViewDidEnter() {
    // this.general.refreshToken();
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
        //this.showPrompt();
        console.log(this.viewCtrl.name);
      } else {
        this.navCtrl.pop();
        console.log(this.viewCtrl.name);
      }
    }, 1);
  }

  ionViewWillLeave() {
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    });
  }

  async verificationLocalData() {
    await this.storage.get("MDC_SAVED").then((dataAll: any) => {
      if (dataAll) {
        if (dataAll.length > 0) {
          dataAll.forEach(e => {
            if (e && e.status == 1 && e.latitude) {
              this.digitalCardProvider.saveMDC(e);
            }
          });
        }
      }
    });

    this.contactSaveLocalStorage();
  }

  async contactSaveLocalStorage() {
    try {
      const response: any = await this.digitalCardProvider.getAllContactsLocal(
        1
      );
      this.contacts = response.data;
      this.storage.set("MDC_SAVED", this.contacts);
    } catch (e) {
      // loading.dismiss();
    }
  }
}
