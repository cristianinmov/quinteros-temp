import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CalendarDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar-detail',
  templateUrl: 'calendar-detail.html',
})
export class CalendarDetailPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
  ) {
  }

  data: any;

  ionViewDidLoad() {
    this.data = this.navParams.get('data');
  }

}
