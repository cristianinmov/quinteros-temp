import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarDetailPage } from './calendar-detail';

@NgModule({
  declarations: [
    CalendarDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CalendarDetailPage),
  ],
})
export class CalendarDetailPageModule {}
