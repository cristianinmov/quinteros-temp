import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OcrPage } from './ocr';
import { NgProgressModule } from '@ngx-progressbar/core';

@NgModule({
  declarations: [
    OcrPage,
  ],
  imports: [
    IonicPageModule.forChild(OcrPage),
    NgProgressModule.forRoot()
  ],
})
export class OcrPageModule {}
