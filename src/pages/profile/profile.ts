import { HomePage } from "./../home/home";
import { environment } from "./../../environments/environment";
import { Storage } from "@ionic/storage";

import { FileService } from "./file.service";

import { AuthProvider } from "./../../providers/auth/auth";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  LoadingController,
  ToastController,
  AlertController,
  ActionSheetController,
  MenuController,
  ViewController,
  Select,
  DateTime
} from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { TranslateService } from "@ngx-translate/core";
import { StatusBar } from "@ionic-native/status-bar";

//FileTransfer

import { File, FileEntry } from "@ionic-native/file";
import { LoginPage } from "../login/login";
import { GeneralProvider } from "../../providers/general/general";
import introJs from '../../../node_modules/intro.js/intro';


//Form Validation
import { Validators, FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { IonicSelectableComponent } from 'ionic-selectable';
import { DigitalCardProvider } from "../../providers/digital-card/digital-card";
import { Sim } from "@ionic-native/sim";

@IonicPage()
class Indicative {
  public id: number;
  public name: string;
  public phonecode: number;
}

/**
 * Componente de presentación para la edición de la información de perfil
 * del usuario en sesión
 */
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  /**
   * Referencia al selector de codigo de país
   */
  @ViewChild("port2") portComponent: IonicSelectableComponent;
  /**
   * @ignore
   * @todo se debe verificar si esta variable se está usando
   */
  ionicSelectableComponent: IonicSelectableComponent;
  /**
   * Objeto con la información del indicativo del país seleccionado
   */
  indicative: Indicative;
  /**
   * Referencia al selector de cargo
   */
  @ViewChild('myselectM') mySelect : Select
  /**
   * @ignore
   * @todo Esta variable parece que no se utiliza
   */
  @ViewChild('myDataTime') myDataTime : DateTime
  /**
   * @ignore
   * @todo Revisar si es necesario definir una variable adicional para esto
   */
  AUTH = environment.API;
  /**
   * @ignore
   * @todo Esta variable guarda lo mismo que la anterior :(
   */
  API = environment.API;

  /**
   * @ignore
   * @todo Revisar si es necesario definir una variable adicional para esto
   */
  USER_DEFAULT = environment.USER_DEFAULT;

  /**
   * Objeto con la información de usuario en sesión
   */
  profile: any = {
    name: "",
    last_name: "",
    avatar: null,
    phone: "",
    email: "",
    password: "",
    country_iso: ''
  };

  /**
   * Identificador del cargo del usuario
   */
  position:any = 0;

  /**
   * Imagen en base64 del usuario utilizado para la previsualización
   */
  base64Image;

  /**
   * @ignore
   * @todo Esta variable al parecer no se esta usando
   */
  photos;

  /**
   * Texto de espera en el lenguaje del usuario en sesión
   * @ignore
   */
  wait: any;

  /**
   * Texto "opción" en idioma del usuario
   * @ignore
   */
  option: any;

  /**
   * Variable temporal para guardar el data uri
   * @ignore
   */
  imageURI: any;

  /**
   * @ignore
   * Esta variable al parecer no se usa
   */
  imageFileName: any;
  
  /**
   * Indica con que método ingreso el usuario a la aplicación
   */
  METHOD_AUTH: any;
  /**
   * texto de traducción
   * @ignore 
   */
  MESSAGE_ERROR: any;
  /**
   * Texto de traducción
   * @ignore 
   */
  GENRAL_TRANSLATE: any;
  /**
   * Texto de traducción
   * @ignore 
   */
  DATA:any;
  /**
   * Indica si el usuario ingresa por primera vez
   */
  firstLogin: any;
  /**
   * Listado de posiciones disponibles para el usuario
   */
  positions:any;
  /**
   * @ignore
   */
  selectOptions:any = {
    cssClass: "alertErrors",
    mode: 'md'
  }
  /**
   * Variable de estado que permite cambiar el tipo del campo contraseña con el fin de permitir ver la contraseña
   * si se da clic en el botón mostrar contraseña
   */
  passwordType: string = 'password';
  /**
   * Clase para el icono que acompaña al botón mostrar contraseña
   */
  passwordIcon: string = 'eye-off';

  /**
   * @ignore
   */
  public profileForm: FormGroup;
  /**
   * Traducciones para mensajes de error
   */
  validation_messages:any;
  /**
   * Prefijo seleccionado del teléfono
   */
  prefixPhone: any;
  /**
   * Variable de estado que indica si el modal de selección de país se encuentra abierto.
   */
  openModal:any;

  /**
   * Arreglo que contiene los prefijos de país disponibles
   */
  prefixArray:any = [];

  /**
   * Prefijo de país establecido por defecto
   */
  prefixInitial:any;
  /**
   * ISO 3166-1 alfa-2 del país seleccionado
   */
  country_iso:any;

  /**
   * 
   * @param fileService Servicio de gestion de archivos
   * @param navCtrl Servicio de manejo de navegación de angular
   * @param navParams Servicio de manejo de parametros de navegación de angular
   * @param authProvider Servicio de manejo de autenticación
   * @param userProvider Servicio de información del usuario en sesión
   * @param storage Servicio encargado de manejar el almacenamiento local
   * @param platform Servicio que provee información de la plataforma
   * @param camera Servicio que provee acceso a la funcionalidad nativa de la camara
   * @param translate Servicio que provee acceso a la traducción
   * @param loadingCtrl Servicio que gestiona la visualización del mensaje de carga de la aplicación
   * @param toastCtrl Servicio que gestiona las notificaciones en la aplicación
   * @param alertCtrl Servicio que gestiona las alertas de la aplicación
   * @param statusBar Servicio que gestiona la barra de estado del dispositivo
   * @param file Servicio que gestiona los archivos del dispositivo
   * @param toastController Servicio que gestiona las notificaciones en la aplicación
   * @param menuCtrl Servicio que gestiona el menu lateral de la aplicación
   * @param actionsheetCtrl Servicio que gestiona la barra de acciones del dispositivo
   * @param general Servicio con utilitarios
   * @param viewCtrl Servicio que gestiona la vista actual de la aplicación
   * @param formBuilder FormBuilder de angular
   * @param digitalCardProvider Servicio que provee el acceso al los servicios API REST de la tarjeta digital
   * @param sim Servicio que gestiona la información de la SIM CARD del dispositivo
   */
  constructor(
    public fileService: FileService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public userProvider: UserProvider,
    public storage: Storage,
    public platform: Platform,
    public camera: Camera,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public statusBar: StatusBar,
    public navController: NavController,
    public file: File,
    public toastController: ToastController,
    public menuCtrl: MenuController,
    public actionsheetCtrl: ActionSheetController,
    public general: GeneralProvider,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    private digitalCardProvider: DigitalCardProvider,
    private sim: Sim
  ) {
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
    this.firstLogin = this.navParams.get("data");

    //disable side menu
    this.menuCtrl.swipeEnable(false);

    //this.getToken();
    this.getLangMessage();

    //config StatusBar
   //ConfigStatusBar
   this.statusBar.backgroundColorByHexString("#E76114");
   this.statusBar.styleLightContent();

    //get current user (localStorage)
    this.getCurrentUser();
    this.validation_messages = this.DATA.VALIDATIONS;
    this.profileForm = this.formBuilder.group({
      password: new FormControl('', Validators.compose([
        Validators.minLength(6)
      ]))
    });
    this.getFlags();
    this.positions = [
      {
        id: 0,
        it_business_id: 0,
        name: this.DATA.GENERAL.SELECT,
        status: "A"
      }
    ];

  }

  /**
   * Evento ejecutado al momento de que la transición de creación de la vista es terminada
   */
  ionViewDidEnter() {
    this.position = 0;
    this.general.getPositionSelect().then((data:any)=>{
      this.positions = [
        {
          id: 0,
          it_business_id: 0,
          name: this.DATA.GENERAL.SELECT,
          status: "A"
        },
        ...data
      ];
      this.getCurrentUser();
    }).catch((err:any)=>{
    })
    this.storage.get("profile_intro_finish").then((data:any)=>{
      if(!data){
        this.intro();
      }
    })
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {}
      else {
        this.navCtrl.pop();
      }
    }, 1);
  }

  /**
   * Permite obtener el usuario en sesión
   */
  getCurrentUser() {
    this.storage
      .get("user_access")
      .then((data: any) => {
        if(data.country && data.country.length > 0) {
          if(data.country.iso) {
            this.getFlagsUserActual(data.country.iso);
            this.country_iso = data.country.iso.toLowerCase();
            this.position = (data.position[0]) ? data.position[0].id : "";
          } else {
            this.getFlagsUserActual(data.country[0].iso);
            this.country_iso = data.country[0].iso.toLowerCase();
            this.position = (data.position[0]) ? data.position[0].id : "";
          }
        }else{
          const country_iso:string = 'us';
          this.getFlagsUserActual(country_iso);
          this.country_iso = country_iso;
          this.position = (data.position[0]) ? data.position[0].id : "";
        }
        
        data.password = "";
        this.profile = data;

      }).catch((e:any)=>{
      })

    this.storage
      .get("METHOD_AUTH")
      .then((response: any) => {
        this.METHOD_AUTH = response.value;
      });
  }

  /**
   * Obtener las traducciones necesarias para la vista
   */
  async getLangMessage() {
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });

    this.translate.get("DATA.OPTION").subscribe(value => {
      this.option = value;
    });

    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });

    this.translate.get("DATA").subscribe(value => {
      this.GENRAL_TRANSLATE = value;
    });

    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });

    this.getToken();
  }

  /**
   * Obtener el token de acceso al API de servicios inmovtech
   */
  async getToken() {
    const data: any = await this.storage.get("accessToken");
    this.AUTH.AUTH = data;
  }

  /**
   * Activa la selección de foto de perfil tanto desde la galeria como la camara
   */
  openeditprofile() {
    let actionSheet = this.actionsheetCtrl.create({
      title: this.option,
      cssClass: "action-sheets-basic-page",
      buttons: [
        {
          text: this.GENRAL_TRANSLATE.MORE_OPTIONS.TAKE_PHOTO,
          role: "destructive",
          icon: !this.platform.is("ios") ? "ios-camera-outline" : null,
          handler: () => {
            this.captureImage(false);
          }
        },
        {
          text: this.GENRAL_TRANSLATE.MORE_OPTIONS.CHOOSE_PHOTO,
          icon: !this.platform.is("ios") ? "ios-images-outline" : null,
          handler: () => {
            this.captureImage(true);
          }
        }
      ]
    });
    actionSheet.present();
  }

  /**
   * si useAlbum es definido como verdadero lanza la galeria definida por defecto en el sistema operativo
   * de lo contrario abre la camara para tomar una nueva fotografía
   * @param useAlbum define si la foto se obtendrá de la galeria si se envia en false lanzará la camara
   */
  async captureImage(useAlbum: boolean) {
    const options: CameraOptions = {
      quality: 50,
      targetHeight: 800,
      targetWidth: 800,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true
    };
    options.sourceType = useAlbum
      ? this.camera.PictureSourceType.PHOTOLIBRARY
      : this.camera.PictureSourceType.CAMERA;
    try {
      const imageData = await this.camera.getPicture(options);
      this.imageURI = imageData;
      this.uploadFile();
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Mostrar una notificación con un texto dado
   * @param text el texto que será mostrado en la notificación
   */
  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      position: "bottom",
      duration: 3000
    });
    toast.present();
  }

  /**
   * Envia la petición que permite subir la nueva fotografía
   * @param formData el cuerpo de la petición de la carga de fotografía
   */
  async sendImageData(formData: FormData) {
    this.storage
      .get("user_access")
      .then((data: any) => {
        this.fileService.upload(formData, data.id).subscribe(result => {
          if (typeof result.error === "undefined") {
            this.profile = result;
            this.storage.set("user_access", result).then(() => {
              this.getCurrentUser();
            });
            this.presentToast(
              this.GENRAL_TRANSLATE.GENERAL_MESSAGES.OK.UPDATED_SUCCESSFULLY
            );
          } else {
            this.presentToast(
              this.GENRAL_TRANSLATE.GENERAL_MESSAGES.ERROR
                .ERROR_WITH_CONECT_SERVE
            );
          }
        });
      })
      .catch((data: any) => {
      });
  }


  /**
   * Genera un blob a partir de la imagen para posteriormente lanzar la carga del blob
   * @param file archivo a partir del cual se generará el blob
   */
  private readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      formData.append("file", imgBlob, file.name);
      this.sendImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }


  /**
   * Obtiene la url de sistema para la fotografía seleccionada por el usuario
   * para posteriormente iniciar la generación del blob
   */
  async uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    try {
      this.file
        .resolveLocalFilesystemUrl(this.imageURI)
        .then(entry => {
          (<FileEntry>entry).file(file => this.readFile(file));
          loader.dismiss();
        })
        .catch(err => this.presentToast("Error"));
    } catch (e) {
      loader.dismiss();
    }
  }

  /**
   * Actualiza la información del usuario en sesión de acuerdo
   * a los datos completados por el usuario en el formulario
   */
  async saveInfo() {
    if(!this.profileForm.valid){
      Object.keys(this.profileForm.controls).forEach(field => {
        const control = this.profileForm.get(field);
        control.markAsTouched({ onlySelf: true });
      });
      return;
    }

    //MessageToast
    let loading = this.loadingCtrl.create({
      content: this.wait
    });
    loading.present();

    //Object of request
    var objData = {
      ...this.profile,
      name: this.profile.name,
      last_name: this.profile.last_name,
      id: this.profile.id,
      birthday: this.profile.birthday,
      password: this.profile.password,
      phone: this.profile.phone,
      position: this.position,
      email: this.profile.email,
      country_iso: this.country_iso
    };
    try {
      const data: any = await this.userProvider.updateInfo(objData);
      data.user.company = data.company;
      const toast = await this.toastController.create({
        message: this.GENRAL_TRANSLATE.GENERAL_MESSAGES.OK.SAVED_SUCCESSFULLY,
        position: "bottom",
        duration: 3000
      });
      data.user.position = data.position;
      // data.user.country.iso = this.country_iso
      await this.storage.set("user_access", data.user);

      this.getCurrentUser();
      toast.present();
      loading.dismiss();

      if (this.firstLogin) {
        this.navCtrl.setRoot(
          HomePage,
          {},
          { animate: true, direction: "forward" }
        );
      }
      this.profile.password = ""
    } catch (e) {
      loading.dismiss();

      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);

            }
          }
        break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  /**
   * Muestra un tour interactivo cuando un usuario ingresa a la sección de
   * edición de perfil
   */
  intro() {
    let me = this;
    let intro = introJs();
    intro.setOptions({
    steps: [
      {
        element: '#profile_avatar_change',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.PROFILE.CANGE_AVATAR+"</span>",
        position: 'bottom'
      }
    ],
    showBullets: false,
    showStepNumbers: false,
    hidePrev: true,
    hideNext: true,
    nextLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.NEXT+"</span>",
    prevLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.PREVIOUS+"</span>",
    skipLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.SKIP+"</span>",
    doneLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.DONE+"</span>"
    });
    intro.oncomplete(function(){
      me.storage.set("profile_intro_finish", true);
    });
    intro.onexit(function() {
      me.storage.set("profile_intro_finish", true);
    });
    intro.start();
  }


  /**
   * Evento lanzado al momento de abrir un selector, registra
   * el evento del botón regresar del dispositivo
   */
  public openSelect(): void{
    let me = this;
    this.platform.registerBackButtonAction(() => {
      // Get the reference to the clear button of Datetime picker.
      var pickerClearButton = document.getElementsByClassName("picker-button")[0];

      // Create a click event to be triggered
      var clickEvent = new MouseEvent("click", {
          "view": window,
          "bubbles": true,
          "cancelable": false
      });

      // Trigger the event
      if(pickerClearButton){
        pickerClearButton.dispatchEvent(clickEvent);
      }
      if(me.mySelect.close()){
        this.platform.registerBackButtonAction(() => {
          if (me.viewCtrl.name == "HomePage") {
            //this.showPrompt();
            
          } else {
            me.navCtrl.pop();
          }
        },1)
      }

  }, 1);
  }

  /**
   * Evento lanzado al momento de cancelar la selección sobre un selector,
   * encargado de registrar el evento del botón regresar del dispositivo
   */
  onCancel(){
    let me = this;
    this.platform.registerBackButtonAction(() => {
      if (me.viewCtrl.name == "HomePage") {
      } else {
        me.navCtrl.pop();
      }
    },1)
  }

  /**
   * Manejador de evento para el evento de cambio de valor,
   * encargado de registrar el evento del botón regresar del dispositivo
   */
  onChange(){
    let me = this;
    this.platform.registerBackButtonAction(() => {
      if (me.viewCtrl.name == "HomePage") {
        //this.showPrompt();
      } else {
        me.navCtrl.pop();
      }
    },1)
  }

  /**
   * Cambiar la visibilidad de la contraseña
   */
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  /**
   * Manejador para el evento de cambio de prefijo de país
   * @param event 
   */
  portChange(event: { component: IonicSelectableComponent; value: any }) {
    this.prefixPhone = event.value.phonecode;
    this.openModal = 0;
    this.platform.registerBackButtonAction(() => {
      this.navController.pop();
    }, 1);
  }

  /**
   * Manejador para el evento de cierre del modal de selección de prefijo de país 
   * @param event Evento que contiene el valor del iso seleccionado
   */
  onClose(event: { component: IonicSelectableComponent; value: any }) {
    this.country_iso = event.component.value.iso.toLowerCase();
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    }, 2);
  }


  /**
   * Manejador para el evento de busqueda del selector de prefijo de país
   * @param event Evento que contiene los criterios de busqueda
   */
  async searchPorts(event:{
    component: IonicSelectableComponent,
    text: string
  }){
    event.component.startSearch();
    if(event.text.length >= 3){
      try{
        const flags:any = await this.digitalCardProvider.getFlagsByName(event.text)
        if(flags.data.length > 0){
          this.prefixArray = flags.data;
        }else{
          this.prefixArray = [];
        }
      }
      catch(e){

        switch (e.status) {
          case 401:
            this.storage.remove("user_access");
            this.storage.remove("METHOD_AUTH");
            this.storage.remove("accessToken");
            this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: "forward" });
            e.error.errors.messages.forEach(element => {
              let options = {
                title: "Oops!",
                subTitle: element,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            });

            break;

          case 422:
            let myText = "";
            let array = Object.keys(e.error.errors).map(key => {
              myText = myText + " " + e.error.errors[key][0];
            });
            for (let index = 0; index < array.length; index++) {
              if (index + 1 == array.length) {
                let options = {
                  title: "Oops!",
                  subTitle: myText,
                  buttons: ["OK"],
                  cssClass: "alertErrors"
                }
                this.general.presentAlertError(options, this.viewCtrl.name);
              }
            }
            break;
          default:
          let options = {
            title: "Oops!",
            subTitle: this.MESSAGE_ERROR,
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
          //catch in te interceptor
        }
      }
    }else{
      this.prefixArray = [this.indicative];
    }
    event.component.endSearch();
  }

  /**
   * Inicializar el prefijo de país de acuerdo a la información obtenida de la SIM CARD del dispositivo
   * @param arrayISO arreglo con los prefijos de país validos
   */
  myPrefix(arrayISO) {
    this.sim
      .getSimInfo()
      .then(info => {
        let prefixLocal;
        arrayISO.forEach(element => {
          if (element.iso == info.countryCode.toUpperCase()) {
            prefixLocal = element;
          }
        });

        this.prefixArray.unshift(prefixLocal);
        this.prefixInitial = this.prefixArray;
        this.indicative = this.prefixArray[0];
        this.prefixPhone = this.prefixArray[0].phonecode;
      })
      .catch(error => {
        let prefixLocal;
        arrayISO.forEach(element => {
          let contrycodeee = "co";
          if (element.iso === contrycodeee.toUpperCase()) {
            prefixLocal = element;
          }
        });
        // prefixLocal = {"id":228,"nombre":"Estados Unidos","name":"United States","code":"US","iso":"US","nicename":"United States","iso3":"USA","numcode":840,"phonecode":1,"status":"A","created_at":null,"updated_at":null,"deleted_at":null,"flag_small":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags\/us.png","flag_large":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-large\/us.png","flag_medium":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-medium\/us.png"};
        this.prefixArray.unshift(prefixLocal);
        this.indicative = this.prefixArray[0];
        this.prefixInitial = this.prefixArray;
        this.prefixPhone = this.prefixArray[0].phonecode;

        let toast = this.toastCtrl.create({
          message: error,
          duration: 2000,
          position: "bottom"
        });
        toast.present(toast);
      });
  }

  /**
   * Obtener el listado de paises validos para el sistema con su respectiva información
   * para posteriormente lanzar la detección del prefijo de país del usuario
   */
  async getFlags() {
    try {
      // var getRequest: any = await this.digitalCardProvider.getAllFlags();
      var getRequest = [{"id":47,"nombre":"Colombia","name":"Colombia","code":"CO","iso":"CO","nicename":"Colombia","iso3":"COL","numcode":170,"phonecode":57,"status":"A","created_at":null,"updated_at":null,"deleted_at":null,"flag_small":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags\/co.png","flag_large":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-large\/co.png","flag_medium":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-medium\/co.png"}]
      this.storage.set("flags", getRequest);
      this.myPrefix(getRequest);
    } catch (e) {
    }
  }

  /**
   * Lanza el modal de selección de prefijo de país
   */
  openModalFlags() {
    let me = this;
    this.platform.registerBackButtonAction(() => {
      if (me.portComponent.close()) {
        this.platform.registerBackButtonAction(() => {
          this.navController.pop();
        }, 1);
      }
      me.openModal == 0;
    }, 1);
  }

  /**
   * Obtener la bandera de un país
   * @param iso ISO del país del que se desea obtener la bandera
   */
  async getFlagsUserActual(iso:String){
    try{
      const flags:any = await this.digitalCardProvider.getFlagsByIso(iso)
      this.prefixArray = flags.data;
      this.indicative = this.prefixArray[0];
      this.prefixInitial = this.prefixArray;
      this.prefixPhone = this.prefixArray[0].phonecode;
    }
    catch(e){

      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: "forward" });
          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
        //catch in te interceptor
      }
    }
  }
}
