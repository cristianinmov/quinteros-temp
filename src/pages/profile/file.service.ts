import { Storage } from "@ionic/storage";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "./../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class FileService {
  AUTH = environment.API;
  API = environment.API;
  USER_DEFAULT = environment.USER_DEFAULT;
  USER_ACCESS: any;

  constructor(private http: HttpClient, public storage: Storage) {
    this.getCurrentUser();
  }
  getCurrentUser() {
    this.storage
      .get("user_access")
      .then((data: any) => {
        this.USER_ACCESS = data;
      })
      .catch((data: any) => {
        console.log(data);
      });
  }

  public upload(form_data: FormData, id:number): Observable<any> {
    return this.http.post<any>(
      this.API.API_URL + "api/v1/user/profile_image/update/" + id, form_data,
      {
        headers: new HttpHeaders({
          Authorization: `Bearer ${this.API.AUTH}`
        })
      }
    );
  }

  public uploadImg(form_data: FormData): Observable<any> {
    return this.http.post<any>( this.API.API_URL + "/api/v1/community/image/upload", form_data,
      {
        headers: new HttpHeaders({
          Authorization: `Bearer ${this.API.AUTH}`
        })
      }
    );
  }
}
