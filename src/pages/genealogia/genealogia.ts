import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GenealogiaDetailPage } from '../genealogia-detail/genealogia-detail';
import { GenealogiaProvider } from '../../providers/genealogia/genealogia';

/**
 * Generated class for the GenealogiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-genealogia',
  templateUrl: 'genealogia.html',
})
export class GenealogiaPage {
  isLoading = true;
  data = []
  search = ''

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public genealogia: GenealogiaProvider,
  ) {
    this.loadFromApi();  
  }

  loadFromApi() {
    this.isLoading = true;
    this.genealogia.getMembers(this.search).subscribe(({ data }: any) => {
      this.isLoading = false;
      this.data = data;
    }, () => {
      
    });
  }

  handleSearch(event) {
    this.search = event.target.value;
    this.loadFromApi();
  }


  openDetail(id) {
    this.navCtrl.push(GenealogiaDetailPage, {
      data: {
        id,
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GenealogiaPage');
  }

}
