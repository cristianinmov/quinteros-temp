import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GenealogiaPage } from './genealogia';
import { GenealogiaDetailPageModule } from '../genealogia-detail/genealogia-detail.module';
import { GenealogiaProvider } from '../../providers/genealogia/genealogia';

@NgModule({
  declarations: [
    GenealogiaPage,
  ],
  providers: [
    GenealogiaProvider,
  ],
  imports: [
    IonicPageModule.forChild(GenealogiaPage),
    GenealogiaDetailPageModule,
  ],
})
export class GenealogiaPageModule {}
