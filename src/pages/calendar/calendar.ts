import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { CalendarDetailPage } from '../calendar-detail/calendar-detail';
import { PagesCalendarApiCalendarProvider } from '../../providers/pages-calendar-api-calendar/pages-calendar-api-calendar';

/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public calendar: PagesCalendarApiCalendarProvider,
    public alertCtrl: AlertController,
  ) {
  }

  isLoading = true;

  calendarData: any;

  ionViewDidLoad() {
    this.calendar.loadCalendar().subscribe((res: any) => {
      this.calendarData = res.data;
      this.isLoading = false;
    }, async (err) => {
      const alert = await this.alertCtrl.create({
        // @ts-ignore
        header: 'Error',
        subHeader: 'Verfica tu conexión',
        message: 'Error cargando información',
        buttons: ['Cerrar']
      });
  
      await alert.present();
    })
  }

  goDetail(cEvent) {
    this.navCtrl.push(CalendarDetailPage, {
      data: cEvent
    })
  }

}
