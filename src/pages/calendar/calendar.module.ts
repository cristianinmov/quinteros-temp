import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarPage } from './calendar';
import {AccordionModule} from 'ngx-accordion';
import { CalendarDetailPageModule } from '../calendar-detail/calendar-detail.module';
import { TranslateModule } from '@ngx-translate/core';
import { PagesCalendarApiCalendarProvider } from '../../providers/pages-calendar-api-calendar/pages-calendar-api-calendar';

@NgModule({
  declarations: [
    CalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(CalendarPage),
    AccordionModule,
    CalendarDetailPageModule,
    TranslateModule,
  ],
  providers: [
    PagesCalendarApiCalendarProvider,

  ]
})
export class CalendarPageModule {}
