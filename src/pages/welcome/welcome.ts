import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { SplashScreen } from '@ionic-native/splash-screen';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Nav) nav: Nav;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, public platform: Platform, public splashScreen: SplashScreen,) {
    this.splashScreen.show();
  }

  async ionViewDidLoad() {
    const statusSlide:any = await this.storage.get("closeSlide")
    this.navCtrl.setRoot(LoginPage);
  }

  nextSlide(){
    this.slides.slideNext();
  }
  closeSlides(status:number){
    this.storage.set("closeSlide", true);
    this.navCtrl.setRoot(LoginPage);
  }

}
