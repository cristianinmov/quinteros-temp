import { AppPermissionComponent } from './../../components/app-permission/app-permission';
import { Globalization } from "@ionic-native/globalization";
import { HomePage } from "./../home/home";
import { Component, ViewChild } from "@angular/core";
import { Slides } from 'ionic-angular';

import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  Nav,
  ToastController,
  AlertController,
  LoadingController,
  MenuController
} from "ionic-angular";
import { RegisterComponent } from "../../components/register/register";
import { StatusBar } from "@ionic-native/status-bar";
import { AuthProvider } from "../../providers/auth/auth";
import { Storage } from "@ionic/storage";
import { environment } from "./../../environments/environment";
import { TranslateService } from "@ngx-translate/core";
import { Device } from "@ionic-native/device";
import { Observable } from "rxjs";
import { Platform } from "ionic-angular";
import { MyApp } from "../../app/app.component";

// ForgotPassword Modal
import { ForgotPaswordComponent } from "../../components/forgot-pasword/forgot-pasword";

// Validators
import { Validators, FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { SplashScreen } from "@ionic-native/splash-screen";
import { ProfilePage } from "../profile/profile";
import { GeneralProvider } from "../../providers/general/general";

//OneSignal
import { OneSignal } from "@ionic-native/onesignal";
import { BlogDetailComponent } from "../../components/blog-detail/blog-detail";
import { CommunityDetailComponent } from "./../../components/community-detail/community-detail";
import { ViewController } from "ionic-angular";
import { AppVersion } from '@ionic-native/app-version';

import { Market } from '@ionic-native/market';

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  /**
   * Referencia al componente de los slides de bienvenida
   */
  @ViewChild(Slides) slides: Slides;

  /**
   * Referencia al componente de navegación de ionic
   */
  @ViewChild(Nav) nav: Nav;

  /**
   * Url principal del api inmovtech
   */
  API = environment.API;

  /**
   * Ruta de la imagen por defecto de los usuarios usada cuando estos no han definido una fotografia
   */
  USER_DEFAULT = environment.USER_DEFAULT;

  /**
   * @ignore
   * variable no utilizada?
   */
  me: any = "";

  /**
   * @ignore
   */
  message: any = "";

  /**
   * Lenguaje del dispositivo del usuario
   */
  lang: any;

  /**
   * Url habilitadas en la api inmovtech
   */
  AUTH = environment.API;

  /**
   * @ignore
   */
  LOCAL_STORAGE = environment.LOCAL_STORAGE;

  /**
   * Información usada para iniciar sesión
   */
  data;

  /**
   * @ignore
   */
  object;

  /**
   * Correo del usuario
   */
  email;
  /**
   * Contraseña del usuario
   */
  password;

  wait: any;
  MESSAGE_ERROR: any;
  LOCAL_LANG: any;

  DATA: any;
  count: number = 0;
  uuidOnesignal: any = "60950f9aeb369a42";

  loginStatus: any = true;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  platformOS:any;
  versionApp:any;
  messageVerssionUpdate: any;
  appId:any;
  validation_messages:any;
  public myGroup: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private statusBar: StatusBar,
    public auth: AuthProvider,
    public toastCtrl: ToastController,
    private storage: Storage,
    public globalization: Globalization,
    public translate: TranslateService,
    public plt: Platform,
    public device: Device,
    public platform: Platform,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public plataform: Platform,
    public myApp: MyApp,
    public menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    public splashScreen: SplashScreen,
    public general: GeneralProvider,
    private oneSignal: OneSignal,
    private viewCtrl: ViewController,
    public appVersion: AppVersion,
    private market: Market
  ) {
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
      this.validation_messages = value.VALIDATIONS;
    });
    this.splashScreen.show();
    this.getLang();

    this.storage.get("uuidonesignal").then((data: any) => {
      this.uuidOnesignal = data;
    });
    this.platform.ready().then(() => {
      if (this.platform.is("ios")) {
        this.statusBar.overlaysWebView(false);
        this.platformOS = "ios";
      }
      if(this.platform.is("android")){
        this.platformOS = "android";
      }
      this.statusBar.backgroundColorByHexString("#E76114");
      this.statusBar.styleLightContent();
    });

    this.menuCtrl.swipeEnable(false);

    this.platform.ready().then(() =>{
      this.myGroup = this.formBuilder.group({

        email: ["",Validators.compose(
          [
            Validators.required
          ])
        ],
        password: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(6)
        ]))
      });
    })
  }

  async getplatformOS(){
    if (this.platform.is("ios")) {
      this.platformOS = "ios";
    }
    if(this.platform.is("android")){
      this.platformOS = "android";
    }
  }
  async ionViewDidEnter(){
    await this.getplatformOS();
    await this.appVersion.getVersionNumber().then((val:any) =>{
      this.versionApp = val;
    });
    await this.appVersion.getPackageName().then((response:any)=>{
      this.appId = response;
    })
    var body = await {"platform": this.platformOS}
    try{
      const response:any = await this.general.getVersionApp(body);
      if(Number(this.versionApp.split('.').join("")) < Number(response.extra.split('.').join(""))) {
        let options = {
          title: this.DATA.LOGIN.APP_VERSIOM_UPDATE.TITLE,
          subTitle: this.DATA.LOGIN.APP_VERSIOM_UPDATE.TEXT_1 + this.DATA.LOGIN.APP_VERSIOM_UPDATE.TEXT_2 + this.versionApp + "<br>" +this.DATA.LOGIN.APP_VERSIOM_UPDATE.TEXT_3 + response.extra,
          buttons: [
            {
              text: this.DATA.LOGIN.APP_UPDATE.CANCEL,
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: this.DATA.LOGIN.APP_UPDATE.UPDATE,
              handler: () => {
                this.market.open(this.appId);
              }
            }
          ],
          cssClass: "alertErrors"
        }
        this.general.updateApp(options, this.viewCtrl.name);
      }

    }
    catch (e){
      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            if (this.count < 3) {
              let options = {
                title: "Oops!",
                subTitle: element,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }

            if (this.count >= 3) {
              this.translate.get("DATA").subscribe(value => {
                this.DATA = value;
                let options = {
                  title: "Oops!",
                  subTitle: element,
                  buttons: [
                    {
                      text: this.DATA.LOGIN.FORGOT_PASSWPRD,
                      handler: () => {
                        let modal = this.modalCtrl.create(
                          ForgotPaswordComponent,
                          {
                            callback: this.dataInfo,
                            email: this.email
                          }
                        );
                        modal.present();
                      }
                    },
                    "OK"
                  ],
                  cssClass: "alertErrors"
                }
                this.general.presentAlertError(options, this.viewCtrl.name);

              });
            }
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  async slideStatus(){

    if (this.platform.is("android")) {
      this.storage.get("permissions_modal").then(val => {
        if (val == null) {
          let permissionModal = this.modalCtrl.create(AppPermissionComponent);
          permissionModal.present();
        }
      });
    }
  }

  ionViewDidLoad() {
    this.storage.get("accessToken").then((data: any) => {
      this.slideStatus();
      if (data != null) {
        this.API.AUTH = data;
        this.navCtrl.setRoot(HomePage,{},{ animate: true, direction: "forward" });
      } else {
        this.splashScreen.hide();
      }
    });
  }

  async getLangMessage() {
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });

    this.translate.get("DATA.LOGIN.HI").subscribe(value => {
      this.message = value;
    });
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
      this.validation_messages = value.VALIDATIONS;
    });
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });

    this.translate.get("LOCAL").subscribe(value => {
      this.LOCAL_LANG = value;
    });
  }

  getEmail() {
    this.storage
      .get("email")
      .then(() => {})
      .catch((e: any) => {
        console.log(e);
      });
  }

  async getLang() {
    try {
      const data: any = await this.globalization.getLocaleName();
      this.lang = data.value.charAt(0) + data.value.charAt(1);
      const lang: string = ['es', 'en'].indexOf(this.lang) > -1 ? this.lang : 'es';;
      await this.storage.set("lang", lang);
      this.AUTH.LANG = ['es', 'en'].indexOf(lang) > -1 ? lang : 'es';
      await this.translate.use(lang);
      this.LOCAL_STORAGE.LANGUAGE = lang;
      await this.getLangMessage();
    } catch (e) {
      console.log(e);
      this.getLangMessage();
    }
  }

  goToHome() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: "forward" });
  }

  goToRegister() {
    this.navCtrl.push(RegisterComponent);
  }

  async Login() {
    console.log('Login pasa')
    if(!this.myGroup.valid){
      Object.keys(this.myGroup.controls).forEach(field => {
        const control = this.myGroup.get(field);
        control.markAsTouched({ onlySelf: true });
      });
      return;
    }

    await this.storage.get("uuidonesignal").then((data: any) => {
      this.uuidOnesignal = data;
      console.log(data);
    });
    let loading = this.loadingCtrl.create({
      content: 'Ingresando...',
      dismissOnPageChange: true
    });

    loading.present();

    this.data = {
      password: this.password,
      login: this.email,
      platform: "Android",
      version: "6.0",
      model: "XT1068",
      uuid: "60950f9aeb369a42",
      language: ['es', 'en'].indexOf(this.LOCAL_LANG) > -1 ? this.LOCAL_LANG : 'es',
      time_zone: "America/Bogota"
    };

    if (this.device.platform) {
      this.data = {
        password: this.password,
        login: this.email,
        platform: this.device.platform,
        version: this.device.version,
        model: this.device.model,
        uuid: this.uuidOnesignal ? "60950f9aeb369a42" : "60950f9aeb369a42",
        language: ['es', 'en'].indexOf(this.LOCAL_LANG) > -1 ? this.LOCAL_LANG : 'es',
      };
    }

    try {
      const data: any = await this.auth.logIn(this.data);
      this.storage.set("user_access", data);
      this.storage.set("accessToken", data.accessToken);
      this.storage.set("METHOD_AUTH", { value: "0" });

      loading.dismiss();

      this.AUTH.AUTH = data.accessToken;
      if (this.device.platform) {
        this.oneSignal.startInit(
          "b8395d0b-9776-4ccb-b26b-5773cdc5850f",
          "quintero-38ab4"
        );

        this.oneSignal.inFocusDisplaying(
          this.oneSignal.OSInFocusDisplayOption.InAppAlert
        );

        this.oneSignal.handleNotificationReceived().subscribe((...notif) => {
          console.log("notificacion", notif);
        });

        this.oneSignal.handleNotificationOpened().subscribe((...notif) => {
          // do something when a notification is opened
          console.log("notificacion", notif);
          switch (notif[0].notification.payload.additionalData.section) {
            case "community":
              this.nav.push(CommunityDetailComponent, {
                infoToNotification: notif[0].notification.payload
              });
              break;
            case "news":
              this.nav.push(BlogDetailComponent, {
                infoToNotification: notif[0].notification.payload
              });
              break;
            default:
              this.nav.setRoot(HomePage);
          }
        });

        this.oneSignal.getIds().then(id => {
          console.log("get ids", id);
          this.storage.set("uuidonesignal", id);
          var uuid = {
            "uuid": id.userId,
            "id": data.id,
            "platform" : this.device.platform,
            "model" : this.device.model,
            "version" : this.device.version,
          }
          this.auth.setUuidOfOneSignal(uuid).then((data)=>{
            console.log(data)
          }).catch((e)=>{
            console.log(e)
          })
        });

        this.oneSignal.endInit();
      }

      if (data.first_login) {
        this.goToHome();
      } else {
        this.navCtrl.setRoot(ProfilePage, {
          data: data
        });
      }

      let toast = this.toastCtrl.create({
        message: this.message + data.name,
        duration: 3000,
        position: "button"
      });

      toast.present(toast);

    } catch (e) {
      loading.dismiss();
      this.count++;

      switch (e.status) {
        case 401:
          e.error.errors.messages.forEach(element => {
            if (this.count < 3) {
              let options = {
                title: "Oops!",
                subTitle: element,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }

            if (this.count >= 3) {
              this.translate.get("DATA").subscribe(value => {
                this.DATA = value;
                let options = {
                  title: "Oops!",
                  subTitle: element,
                  buttons: [
                    {
                      text: this.DATA.LOGIN.FORGOT_PASSWPRD,
                      handler: () => {
                        let modal = this.modalCtrl.create(
                          ForgotPaswordComponent,
                          {
                            callback: this.dataInfo,
                            email: this.email
                          }
                        );
                        modal.present();
                      }
                    },
                    "OK"
                  ],
                  cssClass: "alertErrors"
                }
                this.general.presentAlertError(options, this.viewCtrl.name);

              });
            }
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  aadLogin() {

    let loading = this.loadingCtrl.create({
      content: this.wait,
      dismissOnPageChange: true
    });

    loading.present();

    this.auth.login365()
      .then((res: any) => {
        this.storage.get("uuidonesignal").then(async (data: any) => {
          this.uuidOnesignal = data;
          console.log('res', res)
          //const photo = await this.auth.get360Photo(res.userInfo.userId, res.accessToken, ).toPromise();

          let obj = {
            google_token: res.accessToken,
            email: res.userInfo.uniqueId,
            image: `https://graph.windows.net/${res.tenantId}/users/${res.userInfo.userId}/thumbnailPhoto?api-version=1.5`,
            platform: this.device.platform,
            version: this.device.version,
            model: this.device.model,
            uuid: this.uuidOnesignal ? "60950f9aeb369a42" : "60950f9aeb369a42",
            user_lang: this.LOCAL_LANG,
            time_zone: "America/Bogota",
            language: ['es', 'en'].indexOf(this.LOCAL_LANG) > -1 ? this.LOCAL_LANG : 'es',
          };
          this.auth
            .auth_login_social(obj)
            .then((data: any) => {
              this.oneSignal.startInit(
                "b8395d0b-9776-4ccb-b26b-5773cdc5850f",
                "quintero-38ab4"
              );

              this.oneSignal.inFocusDisplaying(
                this.oneSignal.OSInFocusDisplayOption.InAppAlert
              );

              this.oneSignal
                .handleNotificationReceived()
                .subscribe((...notif) => {
                  console.log("notificacion", notif);
                });

              this.oneSignal
                .handleNotificationOpened()
                .subscribe((...notif) => {
                  // do something when a notification is opened
                  console.log("notificacion", notif);
                  switch (
                    notif[0].notification.payload.additionalData.section
                  ) {
                    case "community":
                      this.nav.push(CommunityDetailComponent, {
                        infoToNotification: notif[0].notification.payload
                      });
                      break;
                    case "news":
                      this.nav.push(BlogDetailComponent, {
                        infoToNotification: notif[0].notification.payload
                      });
                      break;
                    default:
                      this.nav.setRoot(HomePage);
                  }
                });

              this.oneSignal.getIds().then(id => {
                console.log(id);
                this.storage.set("uuidonesignal", id);
                var uuid = {
                  "uuid": id.userId,
                  "id": data.id,
                  "platform" : this.device.platform,
                  "model" : this.device.model,
                  "version" : this.device.version,
                }
                this.auth.setUuidOfOneSignal(uuid).then((data)=>{
                  console.log(data)
                }).catch((e)=>{
                  console.log(e)
                })
              });

              this.oneSignal.endInit();
              loading.dismiss();
              this.storage.set("user_access", data);
              this.storage.set("accessToken", data.accessToken);
              this.storage.set("METHOD_AUTH", { value: "1" });

              this.AUTH.AUTH = data.accessToken;
              if (data.first_login) {
                this.goToHome();
              } else {
                this.navCtrl.setRoot(ProfilePage, {
                  data: data
                });
              }

              let toast = this.toastCtrl.create({
                message: this.message + data.name,
                duration: 3000,
                position: "button"
              });

              toast.present(toast);
            })
            .catch((e: any) => {
              loading.dismiss();
              switch (e.status) {
                case 401:
                  e.error.errors.messages.forEach(element => {
                    let options = {
                      title: "Oops!",
                      subTitle: element,
                      buttons: [
                        {
                          text: this.DATA.LOGIN.BTN_REGISTER,
                          handler: () => {
                            this.navCtrl.push(RegisterComponent);
                          }
                        },
                        "OK"
                      ],
                      cssClass: "alertErrors"
                    }
                    this.general.presentAlertError(options, this.viewCtrl.name);

                  });

                  break;

                case 422:
                  let myText = "";
                  let array = Object.keys(e.error.errors).map(key => {
                    myText = myText + " " + e.error.errors[key][0];
                  });
                  for (let index = 0; index < array.length; index++) {
                    if (index + 1 == array.length) {
                      let options = {
                        title: "Oops!",
                        subTitle: myText,
                        buttons: ["OK"],
                        cssClass: "alertErrors"
                      }
                      this.general.presentAlertError(options, this.viewCtrl.name);
                    }
                  }
                  break;
                default:
                let options = {
                  title: "Oops!",
                  subTitle: this.MESSAGE_ERROR,
                  buttons: ["OK"],
                  cssClass: "alertErrors"
                }
                this.general.presentAlertError(options, this.viewCtrl.name);
              }
            });
        });
      })
      .catch(err => {
        console.log('ERRO', err);
        const showErrors = [7, 17, 10, 13, 8, 14, 5, 3, 4, -1, 15, 16];
        if (showErrors.indexOf(err) > -1) {
          let options = {
            title: "Oops!",
            subTitle: this.translate.instant(`DATA.GOOGLE_LOGIN_ERRORS.${err}`),
            buttons: ["OK"],
            cssClass: "alertErrors"
          }
          this.general.presentAlertError(options, this.viewCtrl.name);
        }
        loading.dismiss();
      });
  }

  opneModalForgotPassword() {
    let modal = this.modalCtrl.create(ForgotPaswordComponent, {
      callback: this.dataInfo
    });
    modal.present();
  }

  dataInfo = data => {
    return new Promise((resolve, reject) => {
      (this.email = data.email), (this.password = data.password);
      console.log(data);
      resolve();
    });
  };

  onKey() {
    this.email = this.email.replace(/\s/g, "");
  }
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
}
