import { Device } from '@ionic-native/device';
import { DigitalCardProvider } from './../../providers/digital-card/digital-card';
import { Storage } from "@ionic/storage";
import { environment } from "./../../environments/environment";
import { StatusBar } from "@ionic-native/status-bar";
import { Globalization } from "@ionic-native/globalization";
import { DirectoryPage } from "./../directory/directory";
import { CommunityPage } from "./../community/community";
import { DigitalCardPage } from "./../digital-card/digital-card";
import { BlogPage } from "./../blog/blog";
import { ProfilePage } from "./../profile/profile";
import { Component } from "@angular/core";
import {
  NavController,
  MenuController,
  Platform,
  AlertController,
  ViewController,
  ToastController,
  ModalController
} from "ionic-angular";

import { TranslateService } from "@ngx-translate/core";
//import { AppVersion } from '@ionic-native/app-version';
import { MyApp } from "../../app/app.component";
import { GeneralProvider } from "../../providers/general/general";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AuthProvider } from "../../providers/auth/auth";
import { LoginPage } from "../login/login";
// import $ from "jquery";
import introJs from '../../../node_modules/intro.js/intro';
import { OneSignal } from '@ionic-native/onesignal';
import { CommunityDetailComponent } from '../../components/community-detail/community-detail';
import { BlogDetailComponent } from '../../components/blog-detail/blog-detail';
import { TermsAndConditionsComponent } from '../../components/terms-and-conditions/terms-and-conditions';
import { CalendarPage } from '../calendar/calendar';
import { GenealogiaPage } from '../genealogia/genealogia';

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  API = environment.API;
  USER_DEFAULT = environment.USER_DEFAULT;
  me: any = {
    avatar: null
  };
  AUTH: any = environment.API;
  news: any;
  meAtributes: any[] = [];
  DATA: any;
  count: number = 1;
  toast:any;
  constructor(
    public navCtrl: NavController,
    public globalization: Globalization,
    public storage: Storage,
    public translate: TranslateService,
    private statusBar: StatusBar,
    public myApp: MyApp,
    public menuCtrl: MenuController,
    public general: GeneralProvider,
    public splashScreen: SplashScreen,
    public authProvider: AuthProvider,
    public platform: Platform,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public digitalCardProvider: DigitalCardProvider,
    public oneSignal: OneSignal,
    public device: Device,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController
  ) {
    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();
    if (this.device.platform) {
      this.oneSignal.startInit(
        "b8395d0b-9776-4ccb-b26b-5773cdc5850f",
        "quintero-38ab4"
      );

      this.oneSignal.inFocusDisplaying(
        this.oneSignal.OSInFocusDisplayOption.Notification
      );

      this.oneSignal.handleNotificationReceived().subscribe((...notif) => {
        console.log("notificacion", notif);
      });

      this.oneSignal.handleNotificationOpened().subscribe((...notif) => {
        // do something when a notification is opened
        console.log("notificacion", notif);
        switch (notif[0].notification.payload.additionalData.section) {
          case "community":
          if(notif[0].notification.payload.additionalData.item_id){
            this.navCtrl.push(CommunityDetailComponent, {
              infoToNotification: notif[0].notification.payload
            });
          }else{
            this.navCtrl.push(CommunityPage, {
              infoToNotification: notif[0].notification.payload
            });
          }

            break;
          case "news":
          if(notif[0].notification.payload.additionalData.item_id){
            this.navCtrl.push(BlogDetailComponent, {
              infoToNotification: notif[0].notification.payload
            });
          }
          else{
            this.navCtrl.push(BlogPage, {
              infoToNotification: notif[0].notification.payload
            });
          }

            break;
          default:
            this.navCtrl.setRoot(HomePage);
        }
      });

      this.oneSignal.getIds().then(id => {
        console.log(id);
      });

      this.oneSignal.endInit();
    }
  }

  launchVgi() {
    window.open('http://www.independence.com.co/vgi/sistema-general/', '_blank', 'location=yes');
  }

  intro() {
    let me = this;
    let intro = introJs();
    intro.setOptions({
    steps: [
      {
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.TOUR+"</span>",
      },
      {
        element: '#btn-menu',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.HOME.BTN_MENU+"</span>",
        position: 'bottom'

      },
      {
        element: '#btn-profile',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.HOME.BTN_PROFILE+"</span>",
        position: 'bottom'
      },
      {
        element: '#section-mdc',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.HOME.SEC_MDC+"</span>",
        position: 'bottom'
      },
      {
        element: '#section-community',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.HOME.SEC_COMMUNITY+"</span>",
        position: 'bottom'
      },
      {
        element: '#section-blog',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.HOME.SEC_BLOG+"</span>",
        position: 'bottom'
      },
      {
        element: '#section-directory',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.HOME.SEC_DIRECTORY+"</span>",
        position: 'bottom'
      }
    ],
    showBullets: true,
    showStepNumbers: false,
    hidePrev: true,
    hideNext: true,
    nextLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.NEXT+"</span>",
    prevLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.PREVIOUS+"</span>",
    skipLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.SKIP+"</span>",
    doneLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.DONE+"</span>"
    });
    intro.oncomplete(function(){
      me.storage.set("home_intro_finish", true);
    });
    intro.onexit(function() {
      me.storage.set("home_intro_finish", true);
    });
    // intro.start();
  }
  ionViewDidLoad() {}

  async getFlags() {
    try {
      // var getRequest: any = await this.digitalCardProvider.getAllFlags();
      var getRequest = [{"id":47,"nombre":"Colombia","name":"Colombia","code":"CO","iso":"CO","nicename":"Colombia","iso3":"COL","numcode":170,"phonecode":57,"status":"A","created_at":null,"updated_at":null,"deleted_at":null,"flag_small":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags\/co.png","flag_large":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-large\/co.png","flag_medium":"\/usr\/share\/nginx\/html\/test_project\/publicflag\/flags-medium\/co.png"}]
      this.storage.set("flags", getRequest);

    } catch (e) {
      console.log("error");
    }
  }

  //CATCH WHEN PRESS BUTTON BACK
  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: this.DATA.ACTION_BACK.TITLE,
      message: this.DATA.ACTION_BACK.DESCRIPTION,
      buttons: [
        {
          text: this.DATA.ACTION_BACK.BTN_CANCEL,
          role: "cancel",
          handler: () => {
            console.log("Application exit prevented!");
            this.count = 1;
          },
          cssClass: "alerCtrlBtnPrimary"
        },
        {
          text: this.DATA.ACTION_BACK.BTN_EXIT,
          handler: () => {
            this.platform.exitApp(); // Close this application
          },
          cssClass: "alerCtrlBtnPrimary"
        }
      ]
    });
    prompt.present();
  }

  getCurrentUser() {
    // this.storage
    //   .get("user_access")
    //   .then((data: any) => {
    //     this.me = data;
    //     this.splashScreen.hide();
    //   })
    //   .catch((data: any) => {
    //     console.log(data);
    //   });
  }
  refreshUser() {
    this.storage.get("accessToken").then((data: any) => {
      this.splashScreen.hide();
      this.API.AUTH = data;
      this.general.getStatusTerms().then((status:any)=>{
        switch(status.accept){
          case 0:
          const terms = this.modalCtrl.create(TermsAndConditionsComponent,{
            status: status
          });
          terms.present();
          break;
          case 1:
          break;
        }
      })
      .catch((e:any)=>{
        console.log(e);
      })
      this.authProvider
        .getMe()
        .then((me: any) => {
          this.general.user = me;
          this.validateAccountStatus(me.account_status);
          if (me.status == "A") {
            this.storage.set("user_access", me);
            this.me = me;
            this.getFlags();

          } else {
            let alert = this.alertCtrl.create({
              title: "Oops!",
              subTitle: this.DATA.HOME_PAGE.MESSAGES.NOT_ACTIVE,
              buttons: ["OK"],
              cssClass: "alertErrors"
            });
            alert.present();
            this.storage.remove("user_access");
            this.storage.remove("METHOD_AUTH");
            this.storage.remove("accessToken");
            this.navCtrl.setRoot(LoginPage);
          }

        })
        .catch((e: any) => {
          this.splashScreen.hide();
          switch (e.status) {
            case 401:
            this.storage.remove("user_access");
            this.storage.remove("METHOD_AUTH");
            this.storage.remove("accessToken");
            this.navCtrl.setRoot(LoginPage);
            e.error.errors.messages.forEach(element => {
              const alert = this.alertCtrl.create({
                title: "Oops!",
                subTitle: element,
                buttons: ["OK"],
                cssClass: "alertErrors"
              });
              alert.present();
            });

              break;
          }
          console.log(e);

        });
    });
  }
  ionViewDidEnter() {

    this.refreshUser();

    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
    this.storage.get("home_intro_finish").then((data:any)=>{
      if(!data){
        this.intro();
      }
    })
    this.getToken();
    this.myApp.getImage();
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage" && this.count == 1) {
        this.showPrompt();
        this.count++;
      } else {
        this.navCtrl.pop();
      }
    }, 1);
    //Enabled Menu
    this.menuCtrl.swipeEnable(true);
  }

  async getToken() {
    try {
      const data: any = await this.storage.get("accessToken");
      this.AUTH.AUTH = data;
    } catch (e) {
      console.log(e);
    }
  }

  goToGenealogia() {
    this.navCtrl.push(GenealogiaPage)
  }

  goToProfile() {
    this.navCtrl.push(ProfilePage);
  }
  
  goToCalendar() {
    this.navCtrl.push(CalendarPage);
  }

  goToDigitalCard() {
    this.navCtrl.push(DigitalCardPage);
  }
  goToCommunity() {
    this.navCtrl.push(CommunityPage);
  }
  goToBlog() {
    this.navCtrl.push(BlogPage);
  }
  goToDirectory() {
    this.navCtrl.push(DirectoryPage);
  }
  validateAccountStatus(status:any){
    switch(status){
      case "Active":
        //Cuenta paga actualmente
        this.general.distroyToastNoPay();
      break;
      case "PaymentBlock":
        // con licencia vencida
        this.general.toastNoPay();
      break;
      case "PaymentAlert":
        // cuando quedan 7 dias para que se termine la licencia

      break;
      case "DemoAlert":
        //demo a 7 dias de vencerse
      break;
    }
  }
}
