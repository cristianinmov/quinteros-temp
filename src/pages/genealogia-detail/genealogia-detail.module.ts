import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GenealogiaDetailPage } from './genealogia-detail';

@NgModule({
  declarations: [
    GenealogiaDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(GenealogiaDetailPage),
  ],
})
export class GenealogiaDetailPageModule {}
