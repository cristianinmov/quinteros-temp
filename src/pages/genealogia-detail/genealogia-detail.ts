import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GenealogiaProvider } from '../../providers/genealogia/genealogia';

/**
 * Generated class for the GenealogiaDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-genealogia-detail',
  templateUrl: 'genealogia-detail.html',
})
export class GenealogiaDetailPage {
  isLoading = true;

  data = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public genealogia: GenealogiaProvider) {
    this.genealogia.getDetail(this.navParams.data.data.id).subscribe((res: any) => {
      this.isLoading = false;
      this.data = res;
    }, () => {});
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad GenealogiaDetailPage');
  }

  goParent(id) {
    this.navCtrl.push(GenealogiaDetailPage, {
      data: {
        id,
      }
    })
  }
}
