import { MyApp } from './../../app/app.component';
import { CommunityDetailComponent } from './../../components/community-detail/community-detail';
import { TranslateService } from "@ngx-translate/core";
import { LoginPage } from "./../login/login";
import { Storage } from "@ionic/storage";
import { StatusBar } from "@ionic-native/status-bar";
import { CommunityNewPublicationComponent } from "./../../components/community-new-publication/community-new-publication";
import { CommutyProvider } from "./../../providers/commuty/commuty";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  Events,
  Platform,
  ViewController,
  LoadingController,
  Content,
  ActionSheetController,
  ToastController
} from "ionic-angular";
import { environment } from "../../environments/environment";
import { CommentaryComponent } from "../../components/commentary/commentary";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { GeneralProvider } from "../../providers/general/general";
import introJs from '../../../node_modules/intro.js/intro';

import { ImageLoader } from 'ionic-image-loader';

import $ from "jquery";
import { BlokedUsersComponent } from '../../components/bloked-users/bloked-users';

@IonicPage()
@Component({
  selector: "page-community",
  templateUrl: "community.html"
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommunityPage {
  @ViewChild(Content) content: Content;
  publications: any = [];
  API = environment.API;
  USER_DEFAULT = environment.USER_DEFAULT;
  PUBLICATION_DEFAULT = environment.PUBLICATION_DEFAULT;
  iconPerfilAtributes: any[] = [];
  publicationAtributes: any[] = [];
  me: any = {
    avatar: ""
  };
  MESSAGE_ERROR;
  pageInit: any = 1;
  wait:any;
  loadingAnimation = environment.LOADDING_IMG;
  DATA:any;
  fakeurl:string = "https://dubsism.files.wordpress.com/2017/12/image-not-found.png";
  GENRAL_TRANSLATE:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public commutyProvider: CommutyProvider,
    public statusBar: StatusBar,
    public storage: Storage,
    public alertCtrl: AlertController,
    public events: Events,
    public photoViewer: PhotoViewer,
    public general: GeneralProvider,
    public translate: TranslateService,
    public platform: Platform,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public imageLoader: ImageLoader,
    public actionsheetCtrl : ActionSheetController,
    public toastCtrl: ToastController
  ) {
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.commutyProvider;
    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    this.storage
      .get("user_access")
      .then((data: any) => {
        this.me = data;
      })
      .catch((e: any) => {
        console.log(e);
      });
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    this.getAllPublications();
    this.imageLoader.preload("../../assets/spinner.gif");
    this.translate.get("DATA").subscribe(value => {
      this.GENRAL_TRANSLATE = value;
    });

  }

  ionViewDidEnter() {
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
    this.storage.get("community_intro_finish").then((data:any)=>{
      if(!data){
        this.intro();
      }
    })
    // this.general.refreshToken();
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
        //this.showPrompt();
        console.log(this.viewCtrl.name);
      } else {
        this.navCtrl.pop();
        console.log(this.viewCtrl.name);
      }
    }, 1);
  }

  newPost() {
    this.navCtrl.push(CommunityNewPublicationComponent, {
      callback: this.saved
    });
  }

  saved = data => {
    return new Promise((resolve, reject) => {
      console.log(data);

      // this.content.scrollB
      this.getAllPublications();
      resolve();
    });
  };

  async getAllPublications() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    try {
      const response: any = await this.commutyProvider.getAllPublications(1);
      this.publications = response.data;
      this.content.scrollToTop();
      loading.dismiss();
      await this.storage.set("allPublications", response.data);
    } catch (e) {
      await this.storage.get("allPublications").then((data:any)=>{
        this.publications = data;
        console.log(data);
        console.log($('img-loader'))
      })
      for (let index = 0; index < $('img-loader > img').length; index++) {
        console.log($('img-loader > img')[index])
        console.log(index);
        $('img-loader > img')[index].error(function() {
          console.log("no cargo");
        }).attr( "src", '../../assets/user_default.jpg' );

      }
      loading.dismiss();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  async newLike(item) {
    console.log(item);
    let data = {
      it_publication_id: item.id,
      it_users_id: this.me.id
    };

    try {
      const response: any = await this.commutyProvider.newLike(data);
      item.likes_cant++;
      item.my_like = 1;
      console.log(response);
    } catch (e) {
      console.log(e.error.errors.messages[0]);
    }
  }

  comments(data, index) {
    this.navCtrl.push(CommentaryComponent, {
      community: data,
      callback: this.getData,
      index: index
    });
  }

  getData = data => {
    return new Promise((resolve, reject) => {
      console.log(data);
      this.publications[data].comments_cant++;
      resolve();
    });
  };
  showImageViewer(url: string, allData: any) {
    if (url) {
      let title: string =
        allData.user.name + " " + allData.user.last_name + " - " + allData.text;
      let options = {
        share: true, // default is false
        closeButton: true, // default is true
        copyToReference: true, // default is false
        headers: "",  // If it is not provided, it will trigger an exception
        piccasoOptions: { } // If it is not provided, it will trigger an exception
      };
      this.photoViewer.show(url, title, options);
    }
  }
  async doRefresh(refresher) {
    try {
      const response: any = await this.commutyProvider.getAllPublications(1);
      this.publications = response.data;
      refresher.complete();
      this.pageInit = 1;
    } catch (e) {
      refresher.complete();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: MyApp,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }

  async doInfinite(infiniteScroll) {
    this.pageInit++;
    try {
      const response: any = await this.commutyProvider.getAllPublications(
        this.pageInit
      );
      response.data.forEach(element => {
        this.publications.push(element);
      });

      infiniteScroll.close();
    } catch (e) {/*

      switch (e.status) {
        case 401:

          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            const alert = this.alertCtrl.create({
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            });
            alert.present();
          });

          break;

        case 422:

          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              const alert = this.alertCtrl.create({
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              });
              alert.present();
            }
          }
          break;
        default:
          const alert = this.alertCtrl.create({
            title: "Oops!",
            subTitle: "no conection",
            buttons: ["OK"],
            cssClass: "alertErrors"
          });
          alert.present();*/
      }
  }
  goToDetail(item){
    this.navCtrl.push(CommunityDetailComponent,{
      obj : item
    });
  }

  intro() {
    let me = this;
    let intro = introJs();
    intro.setOptions({
    steps: [
      {
        element: '#new-post',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.COMMUNITY.NEW_POST+"</span>",
        position: 'bottom'
      }
    ],
    showBullets: false,
    showStepNumbers: false,
    hidePrev: true,
    hideNext: true,
    nextLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.NEXT+"</span>",
    prevLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.PREVIOUS+"</span>",
    skipLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.SKIP+"</span>",
    doneLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.DONE+"</span>"
    });
    intro.oncomplete(function(){
      me.storage.set("community_intro_finish", true);
    });
    intro.onexit(function() {
      me.storage.set("community_intro_finish", true);
    });
    intro.start();
  }
  onImageLoad(imgLoader: ImageLoader) {
    // do something with the loader
  }
  report(item:any){
    let actionSheet = this.actionsheetCtrl.create({
      title: this.GENRAL_TRANSLATE.MORE_OPTIONS.TITLE_COMMUNITY,
      cssClass: "action-sheets-basic-page",
      buttons: [
        {
          text: this.GENRAL_TRANSLATE.MORE_OPTIONS.AS_SPAM,
          role: "destructive",
          icon: !this.platform.is("ios") ? "md-alert" : "ios-alert",
          handler: () => {
            this.asSpam(item);
          }
        },
        {
          text: this.GENRAL_TRANSLATE.MORE_OPTIONS.AS_INAPPROPIATE,
          icon: !this.platform.is("ios") ? "md-backspace" : "ios-backspace",
          handler: () => {
            this.asInappropiate(item);
          }
        },
        {
          text: this.GENRAL_TRANSLATE.MORE_OPTIONS.BLOCK + item.user.name,
          icon: !this.platform.is("ios") ? "md-person" : "ios-person",
          handler: () => {
            this.blockUser(item);
          }
        }
      ]
    });
    actionSheet.present();
  }
  async asSpam(item){
    console.log(item);
    var obj = {
      "type" : "P",
      "item_id" : item.id,
      "report" : "S"
    };
    try{
      const spam: any = await this.commutyProvider.postReportPost(obj);
      console.log(spam)
      this.pageInit = 1;
      this.getAllPublications();
      switch(spam.errors.status){
        case 200:
          spam.errors.messages.forEach(element => {

            let toast = this.toastCtrl.create({
              message: element,
              position: "button",
              showCloseButton: true,
              closeButtonText: "Ok",
              duration: 4000,
            });

            toast.present(toast);
          });
        break;
      }
    }
    catch(e){
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  async asInappropiate(item){
    var obj = {
      "type" : "P",
      "item_id" : item.id,
      "report" : "I"
    };
    try{
      const spam: any = await this.commutyProvider.postReportPost(obj);
      console.log(spam)
      this.pageInit = 1;
      this.getAllPublications();
      switch(spam.errors.status){
        case 200:
          spam.errors.messages.forEach(element => {

            let toast = this.toastCtrl.create({
              message: element,
              position: "button",
              showCloseButton: true,
              closeButtonText: "Ok",
              duration: 4000,
            });

            toast.present(toast);
          });
        break;
      }
    }
    catch(e){
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  async blockUser(item){

    var obj = {
      "user_block_id" : item.user.id
    };
    try{
      const spam: any = await this.commutyProvider.postReportUser(obj);
      this.pageInit = 1;
      await this.getAllPublications();

      switch(spam.errors.status){
        case 200:
          spam.errors.messages.forEach(element => {

            let toast = this.toastCtrl.create({
              message: element,
              position: "button",
              showCloseButton: true,
              closeButtonText: "Ok",
              duration: 4000,
            });

            toast.present(toast);
          });
        break;
      }
    }
    catch(e){
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  async blokedUsers(){
    this.navCtrl.push(BlokedUsersComponent, {
      callback: this.getResponseBlokedUsers
    });
  }
  //CallBack fot get info of DirectoryBranchComponent
  getResponseBlokedUsers = data => {
    return new Promise((resolve, reject) => {
      this.pageInit = 1;
      this.content.scrollTop;
      this.getAllPublications();
      resolve();
    });
  };
}
