import { LoginPage } from "./../login/login";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { environment } from "./../../environments/environment";
import { Storage } from "@ionic/storage";
import { StatusBar } from "@ionic-native/status-bar";
import { BlogDetailComponent } from "./../../components/blog-detail/blog-detail";
import { BlogCategoryComponent } from "./../../components/blog-category/blog-category";
import { BlogProvider } from "./../../providers/blog/blog";
import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { LoadingController, Platform, ViewController } from "ionic-angular";
import { GeneralProvider } from "../../providers/general/general";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from "ionic-angular";

import introJs from '../../../node_modules/intro.js/intro';

/**
 * Generated class for the BlogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-blog",
  templateUrl: "blog.html"
})
export class BlogPage {
  AUTH: any = environment.API;
  news: any = [];
  BLOG_DEFAULT: any = environment.BLOG_DEFAULT;
  data: any;
  hiddeTag: false;
  MESSAGE_ERROR: any;
  wait;
  page: any = 1;
  category: any;

  //loadingIMG
  newAtributes: any[] = [];
  DATA:any;
  PUBLICATION_DEFAULT = environment.PUBLICATION_DEFAULT;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public blogProvider: BlogProvider,
    public statusBar: StatusBar,
    public storage: Storage,
    public photoViewer: PhotoViewer,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public general: GeneralProvider,
    public platform: Platform,
    public viewCtrl: ViewController,
  ) {
    //ConfigStatusBar
    this.statusBar.backgroundColorByHexString("#E76114");
    this.statusBar.styleLightContent();

    //loadingIMG
    this.newAtributes.push({
      element: "onerror",
      value: "this.onerror=null;this.src='" + this.BLOG_DEFAULT + "';"
    });


    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });
    this.translate.get("DATA.WAIT").subscribe(value => {
      this.wait = value;
    });
    this.storage.get("categorySelected").then((data:any) =>{
      if(data){
        this.category = data.id;
        this.data = data.name;
      }
    })
    this.getToken();
  }

  ionViewDidEnter() {
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
    this.storage.get("category_selecet_intro_finish").then((data:any)=>{
      if(!data){
        this.intro();
      }
    })
    // this.general.refreshToken();
    this.platform.registerBackButtonAction(() => {
      if (this.viewCtrl.name == "HomePage") {
        //this.showPrompt();
        console.log(this.viewCtrl.name);
      } else {
        this.navCtrl.pop();
        console.log(this.viewCtrl.name);
      }
    }, 1);
  }

  async getToken() {
    const data: any = await this.storage.get("accessToken");
    this.AUTH.AUTH = data;
    this.getBlog();
  }

  removeFilter() {
    this.storage.set("categorySelected", 0);
    this.data = "";
    this.category = 0;
    this.getBlog();
  }

  async getBlog() {
    let loading = this.loadingCtrl.create({
      content: this.wait
    });

    loading.present();
    try {
      const response: any = await this.blogProvider.getNews(1, this.category);
      this.news = response.data;
      loading.dismiss();
      this.storage.set("allBlog", response.data);
    } catch (e) {
      this.storage.get("allBlog").then((data:any)=>{
        this.news = data;
      })
      loading.dismiss();

      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:
        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
      }
    }
  }
  async doRefresh(refresher) {
    try {
      const response: any = await this.blogProvider.getNews(1, this.category);
      this.news = response.data;
      this.page = 1;
      refresher.complete();
    } catch (e) {
      refresher.complete();
      switch (e.status) {
        case 401:
          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            let options = {
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            }
            this.general.presentAlertError(options, this.viewCtrl.name);
          });

          break;

        case 422:
          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              let options = {
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              }
              this.general.presentAlertError(options, this.viewCtrl.name);
            }
          }
          break;
        default:

        let options = {
          title: "Oops!",
          subTitle: this.MESSAGE_ERROR,
          buttons: ["OK"],
          cssClass: "alertErrors"
        }
        this.general.presentAlertError(options, this.viewCtrl.name);
        //catch in te interceptor
      }
    }
  }

  catergoriesPage() {
    this.navCtrl.push(BlogCategoryComponent, {
      data: this.data,
      callback: this.getData,
      category: this.category
    });
  }

  getData = data => {
    return new Promise((resolve, reject) => {
      this.data = data.name;
      this.category = data.id;
      this.storage.set("categorySelected", data)
      this.getByCategory();
      resolve();
    });
  };

  detailById(blog: any, index) {
    this.navCtrl.push(BlogDetailComponent, {
      new: blog.id,
      likes: blog.likes,
      blog: blog,
      callback: this.getDataDetail,
      index: index
    });
  }

  getDataDetail = data => {
    return new Promise((resolve, reject) => {
      if (data.newLike) {
        this.news[data.index].likes_cant++;
      }
      if (data.newView) {
        this.news[data.index].views_cant++;
      }
      resolve();
    });
  };
  showImageViewer(url: string, allData: any) {

    if (url) {
      let title: string = allData.title;
      let options = {
        share: true, // default is false
	      closeButton: true, // default is true
	      copyToReference: true, // default is false
        headers: "",  // If it is not provided, it will trigger an exception
        piccasoOptions: { } // If it is not provided, it will trigger an exception
      };
      this.photoViewer.show(url, title, options);
    }
  }
  async getByCategory() {
    try {
      const response: any = await this.blogProvider.getNews(1, this.category);
      this.news = response.data;
      this.page = 1;
    } catch (e) {
      console.log(e);
    }
  }

  async doInfinite(infiniteScroll) {
    this.page++;
    try {
      const response: any = await this.blogProvider.getNews(
        this.page,
        this.category
      );
      response.data.forEach(element => {
        this.news.push(element);
      });

      infiniteScroll.close();
    } catch (e) {
      /*
      switch (e.status) {
        case 401:

          this.storage.remove("user_access");
          this.storage.remove("METHOD_AUTH");
          this.storage.remove("accessToken");
          this.navCtrl.setRoot(LoginPage);

          e.error.errors.messages.forEach(element => {
            const alert = this.alertCtrl.create({
              title: "Oops!",
              subTitle: element,
              buttons: ["OK"],
              cssClass: "alertErrors"
            });
            alert.present();
          });

          break;

        case 422:

          let myText = "";
          let array = Object.keys(e.error.errors).map(key => {
            myText = myText + " " + e.error.errors[key][0];
          });
          for (let index = 0; index < array.length; index++) {
            if (index + 1 == array.length) {
              const alert = this.alertCtrl.create({
                title: "Oops!",
                subTitle: myText,
                buttons: ["OK"],
                cssClass: "alertErrors"
              });
              alert.present();
            }
          }
          break;
        default:
          const alert = this.alertCtrl.create({
            title: "Oops!",
            subTitle: "no conection",
            buttons: ["OK"],
            cssClass: "alertErrors"
          });
          alert.present();
        //catch in te interceptor

      }*/
    }
  }
  intro() {
    let me = this;
    let intro = introJs();
    intro.setOptions({
    steps: [
      {
        element: '#select-category',
        intro: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #222;'>"+this.DATA.TOUR.BLOG.CATEGORIES+"</span>",
        position: 'bottom'
      }
    ],
    showBullets: false,
    showStepNumbers: false,
    hidePrev: true,
    hideNext: true,
    nextLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.NEXT+"</span>",
    prevLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.PREVIOUS+"</span>",
    skipLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.SKIP+"</span>",
    doneLabel: "<span style='margin: 0 0 2px;font-size: 1.3rem;font-weight: normal;line-height: 1.5;color: #E76114;'>"+this.DATA.TOUR.DONE+"</span>"
    });
    intro.oncomplete(function(){
      me.storage.set("category_selecet_intro_finish", true);
    });
    intro.onexit(function() {
      me.storage.set("category_selecet_intro_finish", true);
    });
    intro.start();
  }
}
