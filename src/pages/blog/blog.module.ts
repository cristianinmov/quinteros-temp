import { Nowrap } from './../../app/pipes/nowrap';
import { SelecetCategory } from './../../app/pipes/selectCategory';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogPage } from './blog';
import { HttpClientModule, HttpClient  } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicImageLoader } from 'ionic-image-loader';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    BlogPage,
    SelecetCategory,
    Nowrap
  ],
  imports: [
    IonicPageModule.forChild(BlogPage),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    IonicImageLoader,
  ],
})
export class BlogPageModule {}
