import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

/*
  Generated class for the PagesCalendarApiCalendarProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PagesCalendarApiCalendarProvider {

  constructor(public http: HttpClient) {
    
  }

  loadCalendar() {
    return this.http.get(`${environment.API.API_URL}/api/v1/schedules/days/list?filter&filterValue=&page=1&pageSize=100&sortField=date&sortOrder=asc`)
  }

}
