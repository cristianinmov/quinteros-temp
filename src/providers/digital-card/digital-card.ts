import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";

/*
  Generated class for the DigitalCardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DigitalCardProvider {
  API = environment.API;
  AUTH: any = environment.API;

  constructor(public http: HttpClient) { }

  saveMDC(data) {

    return new Promise((resolve, reject) => {

      this.http.post<any>(this.API.API_URL + this.API.DIGITAL_CARD.POST_ADD_CONTACT, data, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err);
        })
    })
  }

  getAllContacts(page:number) {
    return new Promise((resolve, reject) => {
      let urlString =
        "?filterColumn=*&filterValue=&page="+page+"&pageSize=5&sortOrder=desc&sortField=updated_at";
      this.http
        .get(this.API.API_URL + this.API.DIGITAL_CARD.GET_CONTACTS + urlString, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  getAllContactsLocal(page:number) {
    return new Promise((resolve, reject) => {
      let urlString =
        "?filterColumn=*&filterValue=&page="+page+"&pageSize=20&sortOrder=desc&sortField=updated_at";
      this.http
        .get(this.API.API_URL + this.API.DIGITAL_CARD.GET_CONTACTS + urlString, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  sendSMSTwilio(data) {

    return new Promise((resolve, reject) => {

      this.http.post<any>(this.API.API_URL + this.API.API_SMS_TWILIO, data, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err);
        })
    })
  }

  getAllFlags(){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.DIGITAL_CARD.GET_PREFIX, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }
  getFlagsByName(name){
    return new Promise((resolve, reject) => {
      this.http.get(this.API.API_URL + "/api/v1/global/flags?filterColumn=name&filterValue="+name+"&page=1&pageSize=300&sortField=name&sortOrder=asc", {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  getFlagsByIso(name){
    return new Promise((resolve, reject) => {
      this.http.get(this.API.API_URL + "/api/v1/global/flags?filterColumn=iso&filterValue="+name+"&page=1&pageSize=300&sortField=name&sortOrder=asc", {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

}
