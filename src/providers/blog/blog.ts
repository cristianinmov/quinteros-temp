import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";

/*
  Generated class for the BlogProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BlogProvider {
  API = environment.API;
  AUTH: any = environment.API;

  constructor(public http: HttpClient) {
    //console.log("Hello BlogProvider Provider");
  }

  getNews(page, category) {
    return new Promise((resolve, reject) => {
      let urlString;
      if (!category) {
        urlString =
          "?filterValue=&page=" +page +"&pageSize=8&sortField=creation_date&sortOrder=desc";
      } else {
        urlString =
          "?filterValue=" +category +"&page=" +page +"&pageSize=8&sortField=creation_date&sortOrder=desc";
      }
      this.http
        .get(this.API.API_URL + this.API.BLOG.BLOG_GET + urlString, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  getAllCategories() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.BLOG.CATEGORIES_GET, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  getAllCommentaries(id) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.COMMUNITY.GET_ALL_COMMENTARIES + id, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  getNewById(id: any) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.BLOG.BLOG_GET + "/" + id, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
  // getComments(id) {
  //   return new Promise((resolve, reject) => {
  //     this.http
  //       .get(this.API.API_URL + this.API.BLOG.GET_COMMENTS + id, {
  //         headers: new HttpHeaders({
  //           'Accept': "application/json",
  //           "Content-Type": "application/json"
  //         })
  //       })
  //       .subscribe(
  //         res => {
  //           resolve(res);
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }

  // getCommentsOfPage(id) {
  //   return new Promise((resolve, reject) => {
  //     this.http
  //       .get(this.API.API_URL + this.API.BLOG.GET_LIKES_LIKES + id, {
  //         headers: new HttpHeaders({
  //           Accept: "application/json",
  //           "Content-Type": "application/json"
  //         })
  //       })
  //       .subscribe(
  //         res => {
  //           resolve(res);
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }

  // getLikesOfPages(id){
  //   return new Promise((resolve, reject) => {
  //     this.http
  //       .get(this.API.API_URL + this.API.BLOG.GET_LIKES_LIKES + id, {
  //         headers: new HttpHeaders({
  //           Accept: "application/json",
  //           "Content-Type": "application/json"
  //         })
  //       })
  //       .subscribe(
  //         res => {
  //           resolve(res);
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }
  newLike(data: any) {
    return new Promise((resolve, reject) => {
      this.http
        .post(
          this.API.API_URL +
            this.API.BLOG.POST_ADD_LIKES +
            "/" +
            data.it_news_id,
          data,
          {
            headers: new HttpHeaders({
              Accept: "application/json",
              "Content-Type": "application/json"
            })
          }
        )
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  // newView(data: any, id){
  //   return new Promise((resolve, reject) => {
  //     this.http
  //       .post(this.API.API_URL + this.API.BLOG.POST_ADD_VIEW + id, data,  {
  //         headers: new HttpHeaders({
  //           'Accept': "application/json",
  //           "Content-Type": "application/json"
  //         })
  //       })
  //       .subscribe(
  //         res => {
  //           resolve(res);
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }

  //GET GALLERY
  getGalleryById(id){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.BLOG.GALLERY_SHOW_GET + id, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
}
