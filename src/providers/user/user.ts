import { Injectable } from '@angular/core';

import { environment } from './../../environments/environment';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  API = environment.API;

  constructor(public http: HttpClient, public auth : HttpClientModule) { }

  getMe(){
    return new Promise((resolve, reject) => {
      this.http.get(this.API.API_URL + this.API.API_ME, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
      .subscribe(res => {
        resolve(res)
      }, (err) =>{
        reject(err);
      })
    })
  }

  updateInfo(data){
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.API.API_URL + "/api/v1/user/"+ data.id +"?_method=PUT", data, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
      .subscribe(res => {
        resolve(res)
      }, (err) =>{
        reject(err);
      })
    })
  }

}
