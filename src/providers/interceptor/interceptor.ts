import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import {
	HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { ViewChild } from '@angular/core';
import { Nav, ToastController, AlertController } from 'ionic-angular';
import { tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

/*
  Generated class for the InterceptorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InterceptorProvider implements HttpInterceptor{
  @ViewChild(Nav) nav: Nav;
  AUTH = environment.API;
  mobile:string =  'mobile'
  MESSAGE_ERROR:any;
  user_access: any = {
    accessToken:''
  };
  data: string;
  constructor(public storage : Storage,public toastCtrl: ToastController, public alertCtrl: AlertController, public translate : TranslateService) {
    //this.getToken();
    this.translate.get("DATA.NETWORK.MESSAGE_ERROR").subscribe(value => {
      this.MESSAGE_ERROR = value;
    });

  }
    async getToken(){
     const data : any = await this.storage.get("user_access");
     this.data = data.accessToken;
     return this.data;
   }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable< HttpEvent <any> > {
    if (request.headers.get('noToken') !== 'noToken') {
        request = request.clone({
          setHeaders: {
            "Authorization": `Bearer ${ this.AUTH.AUTH }`,
            "X-Language": 'es',
            "X-Platform": "mobile"
          }
        })
    }
    return next.handle(request).pipe(
			tap(event => {
				if (event instanceof HttpResponse) {
					// console.log(event.status);
				}
			}, error => {
        if(error.status == 0){
          // const alert = this.alertCtrl.create({
          //   title: "Oops!",
          //   subTitle: this.MESSAGE_ERROR,
          //   buttons: ["OK"]
          // });
          // alert.present();
        }
        // console.log(error)
        //const err = error.error.message || error.statusText;

				// if (error.status === 401) {

        //   this.storage.remove("user_access");
        //   this.storage.remove("METHOD_AUTH");
        //   this.storage.remove("accessToken");

        // }
        // let toast = this.toastCtrl.create({
        //   message: err,
        //   duration: 2000,
        //   position: 'button'
        // });

        // toast.present(toast);
        return error;
			})
		);
  }
}
