import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

/*
  Generated class for the DirectoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DirectoryProvider {
  API: any = environment.API;

  constructor(public http: HttpClient, public auth : HttpClientModule) {}
  getAllUSers(){
    return new Promise((resolve, reject) => {
      this.http.get(this.API.API_URL + this.API.API_USERS.GET_ALL + "?sortField=name&sortOrder=asc" , {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
      .subscribe(res => {
        resolve(res)
      }, (err) =>{
        reject(err);
      })
    })
  }

  getUserById(id: any){
    return new Promise((resolve, reject) => {
      this.http.get(this.API.API_URL + this.API.API_USERS.GET_BY_ID + id, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
      .subscribe(res => {
        resolve(res)
      }, (err) =>{
        reject(err);
      })
    })
  }

  getAllBraches(){
    return new Promise((resolve, reject) => {
      this.http.get(this.API.API_URL + this.API.DIRECTORY.API_GET_BRANCHES + "?filterColumn=&filterValue=&page=1&pageSize=&sortField=name&sortOrder=asc" , {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
      .subscribe(res => {
        resolve(res)
      }, (err) =>{
        reject(err);
      })
    })
  }

  getUsersByBranch(branch:any){
    return new Promise((resolve, reject) => {
      this.http.get(this.API.API_URL + this.API.API_USERS.GET_ALL + "?filterColumn=it_branches_id&filterValue="+branch.id+"&page=1&pageSize=12&sortField=name&sortOrder=asc" , {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
      .subscribe(res => {
        resolve(res)
      }, (err) =>{
        reject(err);
      })
    })
  }

}
