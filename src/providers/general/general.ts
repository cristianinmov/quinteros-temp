
import { AlertController, ToastController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { AuthProvider } from "./../auth/auth";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Platform } from 'ionic-angular';

import { App } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class GeneralProvider {
  AUTH = environment.API;
  DATA: any;
  count: number = 1;
  toast:any;
  user:any;
  toastShow:any = false;
  constructor(
    public http: HttpClient,
    public authProvider: AuthProvider,
    public storage: Storage,
    public alertCtrl: AlertController,
    public platform: Platform,
    public app: App,
    public translate : TranslateService,
    public toastCtrl: ToastController
  ) {
    this.translate.get("DATA").subscribe(value => {
      this.DATA = value;
    });
  }

  async refreshToken() {
    try {
      this.storage
        .get("user_access")
        .then((user_access: any) => {
          this.authProvider
            .refreshToken()
            .then((response: any) => {
              this.AUTH.AUTH = response.access_token;
              user_access.access_token = response.access_token;
              this.storage.set("accessToken", response.access_token);
              this.storage.set("user_access", user_access);
            })
            .catch((err: any) => {
              console.log(err);
            });
        })
        .catch((e: any) => {
          console.log(e);
        });
    } catch (e) {
      console.log(e);
    }
  }

  setLang(lang:string) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.AUTH.API_URL + this.AUTH.API_SET_LANG + lang, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  getPositionSelect(){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.AUTH.API_URL + this.AUTH.API_POSITIONS, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  async presentAlertError(options:any, page){
    let alert = this.alertCtrl.create({
			title: options.title || '',
			subTitle: options.subTitle || '',
			message: options.message || '',
      buttons: options.buttons || ['OK'],
      cssClass: options.cssClass || '',
    });

    this.platform.registerBackButtonAction(()=>{
      if(alert.dismiss()){
        this.app.navPop();
        console.log(page);
      }
    }, 4);

  	return alert.present();
  }

  toastNoPay(){
    console.log(1, this.toast)
    if(!this.toast){
    console.log(2, this.toast)
      this.toast = this.toastCtrl.create({
        message: this.DATA.LICENS.MESSAGE,
        position: 'top',
        cssClass: "toastNoPaid"
      });

      this.toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
      this.toastShow = true;
      this.toast.present();
      console.log(this.toast);
    }
  }

  distroyToastNoPay(){
    if(this.toastShow){
      this.toast.dismiss()
      this.toast = false;
    }
  }

  getTermsAndConditions(){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.AUTH.API_URL + this.AUTH.API_GET_TERMS_AND_CONDITIONS, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  validateVersionTerms(){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.AUTH.API_URL + this.AUTH.API_VALIDATE_TERMS_AND_CONDITIONS, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  getVersionApp(body){
    return new Promise((resolve, reject) => {
      this.http
        .post(this.AUTH.API_URL + this.AUTH.API_GET_VERSION_APP, body, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err);
        })
    });
  }

  getStatusTerms(){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.AUTH.API_URL + this.AUTH.API_VALIDATE_TERMS_AND_CONDITIONS, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
  agreeTermsAndConditions(){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.AUTH.API_URL + this.AUTH.API_AGREE_TERMS, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  async updateApp(options:any, page){
    let alert = this.alertCtrl.create({
			title: options.title || '',
			subTitle: options.subTitle || '',
			message: options.message || '',
      buttons: options.buttons || ['OK'],
      cssClass: options.cssClass || '',
    });
    alert.onDidDismiss(()=>{
      console.log("saio del aplicativo");
      this.platform.exitApp(); // Close this application
    })

    this.platform.registerBackButtonAction(()=>{
      if(alert.dismiss()){
        console.log("saio del aplicativo");
        this.platform.exitApp(); // Close this application
        console.log(page);
      }
    }, 4);

  	return alert.present();
  }
}
