import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";

/*
  Generated class for the CommutyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommutyProvider {
  API = environment.API;
  AUTH: any = environment.API;

  constructor(public http: HttpClient) { }

  getAllPublications(page:number) {
    return new Promise((resolve, reject) => {
      let urlString =
        "?filterValue=&page="+page+"&pageSize=5&sortField=date&sortOrder=desc";
      this.http
        .get(this.API.API_URL + this.API.COMMUNITY.COMMUNITY_GET + urlString, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  saveNewPublication(data) {

    return new Promise((resolve, reject) => {

      this.http.post<any>(this.API.API_URL + this.API.COMMUNITY.SAVE, data, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err);
        })
    })
  }

  saveComment(id, data) {

    return new Promise((resolve, reject) => {

      this.http.post<any>(this.API.API_URL + this.API.COMMUNITY.POST_ADD_COMMENT + id, data, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err);
        })
    })
  }
  getAllComments(id) {

    return new Promise((resolve, reject) => {
      let string =  "?page=1&pageSize=1000&sortField=created_at&sortOrder=asc"
      this.http.get(this.API.API_URL + this.API.COMMUNITY.GET_ALL_COMMENTARIES + id + string, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err);
        })
    })
  }

  newLike(data){
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.COMMUNITY.POST_ADD_LIKES + "/" + data.it_publication_id, data,  {
          headers: new HttpHeaders({
            'Accept': "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  getCommunityById(id){
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.COMMUNITY.COMMUNITY_GET + "/" + id, {
          headers: new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  postReportUser(user){
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.COMMUNITY.POST_REPORT_USER, user,  {
          headers: new HttpHeaders({
            'Accept': "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
  postReportPost(post){
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.COMMUNITY.POST_REPORT_POST, post,  {
          headers: new HttpHeaders({
            'Accept': "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  getAllBlokedUsers(){
    return new Promise((resolve, reject) => {
      this.http.get( this.API.API_URL + this.API.COMMUNITY.API_GET_BLOKED_USERS, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  postUnlockUsers(data){
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.COMMUNITY.API_POST_UNLOCK_USERS, data,  {
          headers: new HttpHeaders({
            'Accept': "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
}
