import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

/*
  Generated class for the GenealogiaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GenealogiaProvider {
  constructor(public http: HttpClient) {
    console.log('Hello GenealogiaProvider Provider');
  }

  getMembers(search='') {
    return this.http.get(`${environment.API.API_URL}/api/v1/user?filterColumn=&filterValue=${search}&branch=&page=1&pageSize=1000&sortField=name&sortOrder=asc`)
  }
  
  getDetail(id: any) {
    return this.http.get(`${environment.API.API_URL}/api/v1/user/${id}`);
  }
}
