import { Storage } from "@ionic/storage";
import {
  HttpClient,
  HttpClientModule,
  HttpHeaders
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "./../../environments/environment";
import { AuthenticationContext, AuthenticationResult } from '@ionic-native/ms-adal';

import { Observable } from "rxjs/Observable";
import { Platform } from "ionic-angular";

@Injectable()
export class AuthProvider {
  API = environment.API;
  LOCAL_STORAGE = environment.LOCAL_STORAGE;

  constructor(
    public http: HttpClient,
    public auth: HttpClientModule,
    public storage: Storage,
    public plataform: Platform,
  ) {
  }
  getMe() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.API_ME, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }


  async login365() {
    /* let authContext: AuthenticationContext = this.msAdal.createAuthenticationContext('https://login.windows.net/common');

    const authResponse: AuthenticationResult = await authContext.acquireTokenAsync('https://graph.windows.net', '00b57aca-29bb-43f8-a875-7ea3d78980c9', 'https://login.live.com/oauth20_desktop.srf', '', '')
    console.log(authResponse)
    console.log('Token is' , authResponse.accessToken);
    console.log('Token will expire on', authResponse.expiresOn);
    console.log('Token will expire on', authResponse.userInfo);
    return authResponse; */
  }

  get360Photo(userId, token, tenantId) {
    const url = `https://graph.windows.net/${tenantId}/users/${userId}/thumbnailPhoto?api-version=1.6`;
    return this.http.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }

  public logIn(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.API_LOGIN, JSON.stringify(data), {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
            this.API.METHOD = "0";
          },
          err => {
            reject(err);
          }
        );
    });
  }

  register(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(
          this.API.API_URL + this.API.API_REGISTER,
          JSON.stringify(data),
          {
            headers: new HttpHeaders({
              "Accept": "application/json",
              "Content-Type": "application/json"
            })
          }
        )
        .subscribe(
          res => {
            resolve(res);
            this.API.METHOD = "0";
          },
          err => {
            reject(err);
          }
        );
    });
  }
  refreshToken() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.API_REFRESH_TOKEN, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  signOutService() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.API.API_URL + this.API.API_LOGOUT, {
          "headers": new HttpHeaders({
            Accept: "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }


  auth_login_social(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(
          this.API.API_URL + this.API.API_LOGIN_SOCIAL_,
          JSON.stringify(data),
          {
            headers: new HttpHeaders({
              "Accept": "application/json",
              "Content-Type": "application/json"
            })
          }
        )
        .subscribe(
          res => {
            resolve(res);
            this.API.METHOD = "0";
          },
          err => {
            reject(err);
          }
        );
    });
  }

  forgotPasswordSendEmail(email) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.API_FORGOT_PASSWORD, email, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  validateCodeForgotPassword(code) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.API_FORGOT_PASSWORD_VALIDATE, code, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  setUuidOfOneSignal(data){
    return new Promise((resolve, reject) => {
      this.http
        .post(this.API.API_URL + this.API.API_UUID, data, {
          headers: new HttpHeaders({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
}
